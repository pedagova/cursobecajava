package utilities;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;

public class Utilities {

	public static String getUndefined(ResultSetMetaData metaData, ResultSet rs, int i) throws SQLException {
		int index = i+1;
		int type = metaData.getColumnType(index);
		switch (type) {
		case Types.NUMERIC:
			return Integer.toString(rs.getInt(index));
		case Types.VARCHAR:
			return rs.getString(index);
		default:
			System.err.println("undefined type, check the class and complete it, the missing type is: " + type);
		}
		return "";
		
	}
}

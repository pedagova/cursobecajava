package conection;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;

public class Datos {

	public static void infoColumnas(ResultSet rs) throws SQLException {

		ResultSetMetaData metaData = rs.getMetaData();

		// Mostrar en pantalla el n�mero de columnas del ResultSet
		System.out.println("N�mero de columnas: " + metaData.getColumnCount());

		// Mostrar en pantalla el nombre de columnas del ResultSet
		System.out.println("Nombre de las columnas");
		for (int indice = 1; indice <= metaData.getColumnCount(); indice++) {
			System.out.println("\t" + metaData.getColumnName(indice));
		}

		// Mostrar en pantalla el alias de columnas del ResultSet
		System.out.println("Alias de las columnas");
		for (int indice = 1; indice <= metaData.getColumnCount(); indice++) {
			System.out.println("\t" + metaData.getColumnLabel(indice));
		}

		// Mostrar en pantalla el n�mero de columnas del ResultSet
		// que son de tipo cadena (VARCHAR2)
		int total = 0;
		for (int indice = 1; indice <= metaData.getColumnCount(); indice++) {
			if(metaData.getColumnType(indice)==Types.VARCHAR) {
				total++;
			}
		}
		System.out.println("N�mero de columnas de tipo cadena: " +
				total);
	}
	
	public static void mostrarTabla(ResultSet rs) throws SQLException {

		ResultSetMetaData metaData = rs.getMetaData();
		
		//System.out.println("C�DIGO\tNOMBRES\tAPELL\tEDAD\tSALARIO\tESTADO");

		String head = "";
		for (int indice = 1; indice <= metaData.getColumnCount(); indice++) {
			head += ("\t" + metaData.getColumnName(indice));
		}
		System.out.println(head);
		try {
			if(rs.next()) {
				do {
					String rowOutput = "\t";
					for(int i = 0; i < metaData.getColumnCount(); i++) {
						rowOutput += getUndef(metaData, rs, i) + "\t";
					}
					System.out.println(rowOutput);
				} while(rs.next()); // Volver a ejecutar mientras haya registros
			} else {
				System.out.println("No se han devuelto registros");
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	private static String getUndef(ResultSetMetaData metaData, ResultSet rs, int i) throws SQLException {
		int index = i+1;
		int type = metaData.getColumnType(index);
		switch (type) {
		case Types.NUMERIC:
			return Integer.toString(rs.getInt(index));
		case Types.VARCHAR:
			return rs.getString(index);
		default:
			System.err.println("undefined type, check the class and complete it, the missing type is: " + type);
		}
		return "";
		
	}


}

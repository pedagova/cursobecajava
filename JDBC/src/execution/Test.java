package execution;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import conection.Datos;
import conection.JDBC;
import conection.Querys;

public class Test {

	public static void main(String[] args) {
		try {
			// 2.- CARGAR CLASE CON LOS DRIVERS DE BBDD
			Class.forName(JDBC.DRIVERS);
	
		} catch (ClassNotFoundException e) {
			System.out.println("No se puede cargar la clase" + " con los drivers de BBDD");
	
			System.exit(0);
		}
	
		// Conexion a BBDD
		Connection connection = null;
	
		// Ejcucion instruccioneds parametrizadas
		PreparedStatement ps = null;
	
		try {
			/*connection.setAutoCommit(false);
				aqui elemento transitivo
			connection.commit();*/
			
			// 3.- CREAR Y ESTABLECER CONEXION CON LA BBDD
			// Especificamos URL, USUARIO Y CLAVE
			connection = DriverManager.getConnection(JDBC.URL, JDBC.USER, JDBC.PASSWORD);
	
			// 4.- CREAR OBJETO DE EJECUCION DE SQL
			
			System.out.println("-------------AUTORES-------------");
			ps = connection.prepareStatement(Querys.AUTORQUERY);
			
			ResultSet rs = ps.executeQuery();
			
			
			Datos.mostrarTabla(rs);

			
			System.out.println("-------------LIBROS-------------");
			ps = connection.prepareStatement(Querys.BOOKQUERY);				
			
			rs = ps.executeQuery();
			
			
			Datos.mostrarTabla(rs);
			
			
			System.out.println("-------------AUTOR's BOOKs-------------");
			ps = connection.prepareStatement(Querys.FINDBOOKQUERY);		
			
			ps.setString(1, "laura");
			
			rs = ps.executeQuery();
			
			Datos.mostrarTabla(rs);
			
			System.out.println("-------------INSERT-------------");
			
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(Querys.BOOKINSERT);
			ps.setInt(1, 4);
			ps.setString(2, "cancion de hielo y fuego");
			ps.setInt(3, 32);
			ps.setString(4, "2020-12-12");
			ps.setInt(5, 6);
			try {
				ps.executeUpdate();
			}catch(SQLException s) {
				System.err.println("la has liado pollito, inserta bien mamon");
				throw s;
			}
			
			connection.commit();
			
			System.out.println("se ha insertado todo ok");
		
			
			ps.close();
			ps = null;
			
			rs.close();
			rs = null;
	
		} catch (SQLException e1) {
			// Descartar la transacción
			try {
				connection.rollback();
				
				System.out.println("No se ha registrado el pedido!!!!!");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
		} finally {
	
			// 7.- CERRAR CONEXION Y LIBERAR RECURSOS UTILIZADOS
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.out.println("No se puede cerrar conexion " + " con BBDD");
				}
			}
		}
	}
}

	

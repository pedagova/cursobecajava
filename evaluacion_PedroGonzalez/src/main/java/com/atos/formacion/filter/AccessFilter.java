package com.atos.formacion.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import org.springframework.core.annotation.Order;

import com.atos.formacion.modelo.User;

@WebFilter("/admin/*")
@Order(1)
public class AccessFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		System.out.println("inicio");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		
		User u = (User) req.getSession().getAttribute("userAct");
		
		if(u != null && u.getRol().getName().equals("admin")) {
			chain.doFilter(request, response);
		}else {
			RequestDispatcher dispatcher = 
					request.getRequestDispatcher("/user/view");
	
			dispatcher.forward(request, response);
		}

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		System.out.println("fin");
	}

}

package com.atos.formacion.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import org.springframework.core.annotation.Order;

import com.atos.formacion.modelo.User;

@WebFilter("/admin/user/*")
@Order(2)
public class ModifFilter implements Filter {
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		System.out.println("inicio");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		String[] uri = req.getRequestURI().split("/");
		if(!req.getRequestURI().matches("/formacion/admin/user/\\d*")) {
			System.out.println("no ok");
			chain.doFilter(request, response);
			return;
		}
		else {
			System.out.println("ok");
		}
		int index = -1;
		try {
			index = Integer.parseInt((uri[uri.length - 1]));
			User u = (User) req.getSession().getAttribute("userAct");
			
			if(u != null && u.getId() != index) {
				chain.doFilter(request, response);
			}
		} catch (Exception e) {
			System.out.println("pet0");
		}
		
		
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		System.out.println("fin");
	}

}

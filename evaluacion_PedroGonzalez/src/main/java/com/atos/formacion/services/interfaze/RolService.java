package com.atos.formacion.services.interfaze;

import java.util.List;

import com.atos.formacion.modelo.Rol;

public interface RolService {

	void add(Rol rol);

	void update(Rol rol);

	void destroy(int rol);

	Rol getById(int rol);

	List<Rol> getAll();
	
}

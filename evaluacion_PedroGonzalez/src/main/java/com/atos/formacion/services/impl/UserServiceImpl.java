package com.atos.formacion.services.impl;

import java.util.List;

import javax.persistence.NoResultException;
import javax.validation.ConstraintViolationException;

import org.hibernate.NonUniqueResultException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.atos.formacion.dao.interfaze.UserDao;
import com.atos.formacion.dao.interfaze.UserLogDao;
import com.atos.formacion.exceptions.CannotCreateUserException;
import com.atos.formacion.exceptions.CannotUpdateUserException;
import com.atos.formacion.exceptions.UserLogNonFoundException;
import com.atos.formacion.exceptions.UserNonActiveException;
import com.atos.formacion.exceptions.UserNonFoundException;
import com.atos.formacion.exceptions.WrongPassException;
import com.atos.formacion.modelo.Pagnation;
import com.atos.formacion.modelo.User;
import com.atos.formacion.modelo.UserLog;
import com.atos.formacion.services.interfaze.UserService;

@Service
public class UserServiceImpl implements UserService{
	@Autowired
	private UserDao dao;
	@Autowired
	private UserLogDao log;

	@Transactional
	@Override
	public void add(User user) throws CannotCreateUserException, ConstraintViolationException{
		try {
			user.setId(getNextId());
			dao.create(user);
		}catch(ConstraintViolationException e) {
			throw e;
		}catch(Exception e) {
			throw new CannotCreateUserException("El usuario no ha sido creado porque ya existe uno con el id= " + user.getId());
		}
	}

	
	@Transactional
	@Override
	public void update(User user) throws CannotUpdateUserException {
		User u = dao.findById(user.getId());
		if(u == null) {
			throw new CannotUpdateUserException("El usuario no ha sido modificado porque no existe ningun usuario con la id=" + user.getId());
		}
		if(!u.getDashid().equals(user.getDashid()))
			throw new CannotUpdateUserException("El usuario no ha sido modificado porque el identificador de empleado, " + user.getDashid() + " ha sido modificado a: " +  user.getDashid());
		dao.edit(user);
	}

	
	@Transactional
	@Override
	public void destroy(int userId) {
		log.delete(log.findByUserId(userId).getLog());
		dao.delete(userId);
	}

	
	@Transactional(readOnly = true)
	@Override
	public User getById(int userId) {
		return dao.findById(userId);
	}

	
	@Transactional(readOnly = true)
	@Override
	public List<User> getAll() {
		return dao.findAll();
	}
	
	@Transactional(readOnly = true)
	@Override
	public User loggin(String nick, String pass) throws UserNonActiveException, UserLogNonFoundException, UserNonFoundException, WrongPassException {
		
		User u = null;
		try{
			u = dao.findByDashId(nick);
		}catch (NoResultException e) {
			throw new UserNonFoundException("el usuario con el dashId " + nick + "no se ha encontrado en la base de datos");
		}catch(NonUniqueResultException e) {
			throw new UserNonFoundException("el usuario con el dashId " + nick + "tiene el log duplicado");
		}
		if(u.getStatus() == 1)
			throw new UserNonActiveException("El usuario: " + u.getDashid() + "no puede hacer loggin porque esta inactivo");
		UserLog l = null;
		try {
			l = log.findByUserId(u.getId());
		}catch(NoResultException e) {
			throw new UserLogNonFoundException("No se ha encontrado loggin para el usuario con dashId " + u.getDashid());
		}
		if(!l.getPass().equals(pass)) 
			throw new WrongPassException("La pass introucida por el usuario " + u.getDashid() + "es incorrecta");
		return u;
	}

	@Transactional
	@Override
	public User logginCreate(String name, String pass) throws UserNonFoundException {
		User u = null;
		try{
			u = dao.findByDashId(name);
			UserLog uLog = new UserLog();
			uLog.setPass(pass);
			uLog.setUser(u.getId());
			uLog.setLog(getNextLogId());
			
			log.create(uLog);
			return u;
		}catch (NoResultException e) {
			throw new UserNonFoundException("el usuario con el dashId " + name + "no se ha encontrado en la base de datos");
		}	
	}
	
	private int getNextId() {
		int max = 0;
		for(User i : dao.findAll()){
			if(i.getId() > max)
				max = i.getId();
		}
		return max + 1;
	}
	private int getNextLogId() {
		int max = 0;
		for(UserLog i : log.findAll()){
			if(i.getLog() > max)
				max = i.getLog();
		}
		return max + 1;
	}

	@Transactional
	@Override
	public List<User> getAll(String string) {
		if(string!=null && string!="")
			return dao.findAll(string);

		return dao.findAll();
	}

	@Transactional
	@Override
	public Object getAll(Pagnation page) {
		return dao.findAll(page.getMaxResults(), page.getAct());
	}
}

package com.atos.formacion.services.interfaze;

import java.util.List;

import javax.validation.ConstraintViolationException;

import com.atos.formacion.exceptions.CannotCreateUserException;
import com.atos.formacion.exceptions.CannotUpdateUserException;
import com.atos.formacion.exceptions.UserLogNonFoundException;
import com.atos.formacion.exceptions.UserNonActiveException;
import com.atos.formacion.exceptions.UserNonFoundException;
import com.atos.formacion.exceptions.WrongPassException;
import com.atos.formacion.modelo.Pagnation;
import com.atos.formacion.modelo.User;

public interface UserService {
	
	void add(User user) throws CannotCreateUserException, ConstraintViolationException;

	void update(User user) throws CannotUpdateUserException;

	void destroy(int userId);

	User getById(int userId);

	List<User> getAll();
	
	User loggin(String nick, String pass) throws UserNonActiveException, UserLogNonFoundException, UserNonFoundException, WrongPassException;

	User logginCreate(String name, String pass) throws UserNonFoundException;

	List<User> getAll(String string);

	Object getAll(Pagnation page);
	
}

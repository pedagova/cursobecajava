package com.atos.formacion.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.atos.formacion.dao.interfaze.RolDao;
import com.atos.formacion.modelo.Rol;
import com.atos.formacion.services.interfaze.RolService;

@Service
public class RolServiceImpl implements RolService{
	@Autowired
	private RolDao dao;


	@Transactional
	@Override
	public void add(Rol rol) {
		dao.create(rol);
	}

	
	@Transactional
	@Override
	public void update(Rol rol) {
		dao.edit(rol);
	}

	
	@Transactional
	@Override
	public void destroy(int rolId) {
		dao.delete(rolId);
	}

	
	@Transactional(readOnly = true)
	@Override
	public Rol getById(int rolId) {
		return dao.findById(rolId);
	}

	
	@Transactional(readOnly = true)
	@Override
	public List<Rol> getAll() {
		return dao.findAll();
	}
}

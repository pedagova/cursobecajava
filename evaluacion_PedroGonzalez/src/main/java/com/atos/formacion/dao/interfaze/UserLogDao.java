package com.atos.formacion.dao.interfaze;

import java.util.List;

import com.atos.formacion.modelo.UserLog;

public interface UserLogDao  extends GenericDAO<UserLog, Integer>{

	UserLog findByUserId(int id);

	List<UserLog> findAll();

}

package com.atos.formacion.dao.impl;

import javax.validation.Valid;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.atos.formacion.dao.interfaze.GenericDAO;

public class GenericDAOImpl<T, PK> implements GenericDAO<T, PK> {
	
	@Autowired
	protected SessionFactory sessionFactory;
	
	private Class<T> classType;

	public GenericDAOImpl(Class<T> classType) {
		this.classType = classType;
	}
	
	@Override
	public void create(@Valid T entity) {
		sessionFactory.getCurrentSession().save(entity);
	}

	@Override
	public void edit(T entity) {
		sessionFactory.getCurrentSession().merge(entity);
	}

	@Override
	public void delete(PK idEntity) {
		
		sessionFactory.getCurrentSession().delete(findById(idEntity));
	}

	@Override
	public T findById(PK idEntity) {
		return sessionFactory.getCurrentSession().find(classType, idEntity);
	}
	

}

package com.atos.formacion.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.atos.formacion.dao.interfaze.UserLogDao;
import com.atos.formacion.modelo.UserLog;

@Repository
public class UserLogDaoImpl extends GenericDAOImpl<UserLog, Integer> implements UserLogDao{

	public UserLogDaoImpl() {
		super(UserLog.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	public UserLog findByUserId(int id) {
		@SuppressWarnings("unchecked")
		TypedQuery<UserLog> query =
				sessionFactory.getCurrentSession().
				createQuery("from UserLog u where user = '" + id + "'");
		return query.getSingleResult();
	}

	@Override
	public List<UserLog> findAll() {
		@SuppressWarnings("unchecked")
		TypedQuery<UserLog> query =
				sessionFactory.getCurrentSession().
				createQuery("from UserLog");
		return query.getResultList();
	}

	
}


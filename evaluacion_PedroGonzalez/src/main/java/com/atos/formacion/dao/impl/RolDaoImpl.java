package com.atos.formacion.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.atos.formacion.dao.interfaze.RolDao;
import com.atos.formacion.modelo.Rol;

@Repository
public class RolDaoImpl extends GenericDAOImpl<Rol, Integer> implements RolDao{

	public RolDaoImpl() {
		super(Rol.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Rol> findAll() {
		@SuppressWarnings("unchecked")
		TypedQuery<Rol> query =
				sessionFactory.getCurrentSession().
				createQuery("from Rol");
		
		return query.getResultList();
	}

	
}


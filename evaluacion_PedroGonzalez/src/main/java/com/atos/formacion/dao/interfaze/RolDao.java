package com.atos.formacion.dao.interfaze;

import java.util.List;

import com.atos.formacion.modelo.Rol;

public interface RolDao  extends GenericDAO<Rol, Integer>{

	List<Rol> findAll();

}

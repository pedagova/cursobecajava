package com.atos.formacion.dao.impl;



import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.atos.formacion.dao.interfaze.UserDao;
import com.atos.formacion.modelo.User;

@Repository
public class UserDaoImpl extends GenericDAOImpl<User, Integer> implements UserDao{

	public UserDaoImpl() {
		super(User.class);
	}
	
	@Override
	public List<User> findAll(int maxResults, int actPage) {
		int tr = findAll().size();
		int page = 0;
		
		page=actPage % ((int)(tr / maxResults)+1);
		System.out.println("!!!!!!!!!!!!!!!!!!!!");
		System.out.println(page);
		@SuppressWarnings("unchecked")
		TypedQuery<User> query =
				sessionFactory.getCurrentSession().
				createQuery("from User")
				.setFirstResult(page*maxResults)
	            .setMaxResults(maxResults);
		
		return query.getResultList();
	}
	
	@Override
	public List<User> findAll(String string) {
		String[] aux = string.split(" ");
		StringBuilder fin = new StringBuilder();
		fin.append(aux[0]);
		for(String a : aux) {
			fin.append(", " + a);
		}
		@SuppressWarnings("unchecked")
		TypedQuery<User> query =
				sessionFactory.getCurrentSession().
				createQuery("from User order by " + fin.toString());
		
		return query.getResultList();
	}

	@Override
	public User findByDashId(String nick) {
		@SuppressWarnings("unchecked")
		TypedQuery<User> query =
				sessionFactory.getCurrentSession().
				createQuery("from User u where dashId = '" + nick + "'");
		System.out.println(query.getSingleResult());
		return query.getSingleResult();
	}

	@Override
	public List<User> findAll() {
		@SuppressWarnings("unchecked")
		TypedQuery<User> query =
				sessionFactory.getCurrentSession().
				createQuery("from User");
		
		return query.getResultList();
	}

	
}
package com.atos.formacion.dao.interfaze;

import java.util.List;

public interface GenericDAO<T, PK> {

	void create(T entity);
	
	void edit(T entity);
	
	void delete(PK idEntity);
	
	T findById(PK idEntity);

}

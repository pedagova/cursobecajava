package com.atos.formacion.dao.interfaze;

import java.util.List;

import com.atos.formacion.modelo.User;

public interface UserDao extends GenericDAO<User, Integer>{

	List<User> findAll();

	User findByDashId(String nick);

	List<User> findAll(String string);

	List<User> findAll(int maxResults, int actPage);

}

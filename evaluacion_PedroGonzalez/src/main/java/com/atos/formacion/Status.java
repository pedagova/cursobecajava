package com.atos.formacion;

public class Status {
	public static final int ACTIVE = 0;
	public static final int NON_ACTIVE = 1;
}

package com.atos.formacion.modelo.forms;

public class FilterClass {

	private boolean dashId;
	private boolean name ;
	private boolean forename1;
	private boolean forename2 ;
	private boolean rol;
	private boolean status;
	public boolean isDashId() {
		return dashId;
	}
	public void setDashId(boolean dashId) {
		this.dashId = dashId;
	}
	public boolean isName() {
		return name;
	}
	public void setName(boolean name) {
		this.name = name;
	}
	public boolean isForename1() {
		return forename1;
	}
	public void setForename1(boolean forename1) {
		this.forename1 = forename1;
	}
	public boolean isForename2() {
		return forename2;
	}
	public void setForename2(boolean forename2) {
		this.forename2 = forename2;
	}
	public boolean isRol() {
		return rol;
	}
	public void setRol(boolean rol) {
		this.rol = rol;
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if(isDashId())
			sb.append("dashid ");
		if(isName())
			sb.append("name ");
		if(isForename1())
			sb.append("forename1 ");
		if(isForename2())
			sb.append("forename2 ");
		if(isRol())
			sb.append("rol ");
		if(isStatus())
			sb.append("status ");
		System.out.println(sb.toString());
		return sb.toString();
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
}

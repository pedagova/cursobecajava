package com.atos.formacion.modelo.forms;

import org.hibernate.validator.constraints.Email;

public class UserForm {

	private String dashId; 
	private String name ;
	private String forename1; 
	private String forename2;
	private String password;
	private String email;
	private int id;	
	private int status;
	private int rol;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getDashId() {
		return dashId;
	}
	public void setDashId(String dashId) {
		this.dashId = dashId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getForename1() {
		return forename1;
	}
	public void setForename1(String forename1) {
		this.forename1 = forename1;
	}
	public String getForename2() {
		return forename2;
	}
	public void setForename2(String forename2) {
		this.forename2 = forename2;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getRol() {
		return rol;
	}
	public void setRol(int rol) {
		this.rol = rol;
	}
	public UserForm() {
		super();
		this.id = -1;
	}
	public int getId() {
		if(id >= 0)
			return id;
		return -1;
	}
	public void setId(int id) {
		if(id >= 0)
			this.id = id;
	}
}

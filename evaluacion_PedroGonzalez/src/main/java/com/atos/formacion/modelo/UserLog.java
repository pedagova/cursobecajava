package com.atos.formacion.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="USERLOG")
public class UserLog {

	@Id
	@Column(name = "pk_logid")
	private int log;
	
	@Column(name = "fk_userid")
	private int user;
	
	@Column(name = "userpass")
	private String pass;

	public int getLog() {
		return log;
	}

	public void setLog(int log) {
		this.log = log;
	}

	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public UserLog() {
		super();
	}
	
	
	
}

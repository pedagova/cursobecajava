package com.atos.formacion.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

@Entity
@Table(name="USERINFO")
public class User{
	
	@Id
	@Column(name="pk_userid")
	private int id;
	
	@Column(name="status")
	@Range(min = 0, max = 1)
	private int status;
	
	@Column(name="dashid")
	@Pattern(regexp = "[A-Z]\\d*") @Length(min=7, max=7)
	private String dashId;
	
	@Column(name="uname")
	@NotBlank
	private String name;
	
	@Column(name="forename1")
	@NotBlank
	private String forename1;
	
	@Column(name="forename2")
	@NotBlank
	private String forename2;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="fk_rolid")
	@NotNull
	private Rol rol;
	
	@Column(name="email")
	@NotBlank @Email
	private String email;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDashid() {
		return dashId;
	}

	public void setDashid(String dashId) {
		this.dashId = dashId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getForename1() {
		return forename1;
	}

	public void setForename1(String forename1) {
		this.forename1 = forename1;
	}

	public String getForename2() {
		return forename2;
	}

	public void setForename2(String forename2) {
		this.forename2 = forename2;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol2) {
		this.rol = rol2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", status=" + status + ", dashId=" + dashId + ", name=" + name + ", forename1="
				+ forename1 + ", forename2=" + forename2 + ", rol=" + rol.getName() + ", email=" + email + "]";
	}

	public User(int id, int status, String dashId, String name, String forename1, String forename2,
			String email) {
		super();
		this.id = id;
		this.status = status;
		this.dashId = dashId;
		this.name = name;
		this.forename1 = forename1;
		this.forename2 = forename2;
		this.email = email;
	}

	public User() {
		super();
		id=-1;
	}

	
}
	

package com.atos.formacion.modelo;

import org.springframework.stereotype.Component;

@Component
public class Pagnation {

	private int act = 0;
	private int maxResults = 10;
	public int getAct() {
		return act;
	}
	public void setAct(int act) {
		this.act = act;
	}
	public int getMaxResults() {
		return maxResults;
	}
	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}
	public void nextPage() {
		act+=1;
	}
	public void prevPage() {
		act-=1;
	}
	public Pagnation(int act, int maxResults) {
		super();
		this.act = act;
		this.maxResults = maxResults;
	}
	public Pagnation() {
		super();
	}
}

package com.atos.formacion.exceptions;

public class UserNonActiveException extends Exception{

	public UserNonActiveException(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}

package com.atos.formacion.exceptions;

public class UserLogNonFoundException extends Exception{

	public UserLogNonFoundException(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}

package com.atos.formacion.exceptions;

public class UserNonFoundException extends Exception{

	public UserNonFoundException(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}

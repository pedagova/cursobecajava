package com.atos.formacion.exceptions;

public class CannotUpdateUserException extends Exception{

	public CannotUpdateUserException(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}

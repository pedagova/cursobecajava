package com.atos.formacion.exceptions;

public class CannotCreateUserException extends Exception{

	public CannotCreateUserException(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}

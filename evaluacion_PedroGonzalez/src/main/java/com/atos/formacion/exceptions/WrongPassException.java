package com.atos.formacion.exceptions;

public class WrongPassException extends Exception{

	public WrongPassException(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}

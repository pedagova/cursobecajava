package com.atos.formacion.web.controlers;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.atos.formacion.exceptions.CannotCreateUserException;
import com.atos.formacion.exceptions.CannotUpdateUserException;
import com.atos.formacion.modelo.Pagnation;
import com.atos.formacion.modelo.Rol;
import com.atos.formacion.modelo.User;
import com.atos.formacion.modelo.forms.FilterClass;
import com.atos.formacion.modelo.forms.UserForm;
import com.atos.formacion.services.interfaze.RolService;
import com.atos.formacion.services.interfaze.UserService;

/*
 * Componente Spring configurado como controlador MVC
 */
@Controller
public class UsersController {

	@Autowired
	private UserService service;
	@Autowired
	private RolService rolService;

	@ModelAttribute("user")
	public User getUser() {
		return new User();
	}

	@ModelAttribute("rols")
	public List<Rol> getRol() {
		return rolService.getAll();
	}

	@GetMapping("/user/view")
	public ModelAndView view() {
		return new ModelAndView("userView");
	}
	
	@GetMapping("/admin/user/all")
	public ModelAndView index(HttpSession session) {
		try {
			Pagnation page = (Pagnation) session.getAttribute("page");
			ModelAndView m = new ModelAndView("showAll", "users", service.getAll(page));
			m.addObject("filterClass", new FilterClass());
			return m;
		} catch (Exception e) {
			ModelAndView m = new ModelAndView("error");
			m.addObject("msg", "parece ser que el sistema esta caido, por favor, intentelo m�s tarde");
			return m;
		}
	}

	@GetMapping("/admin/user/{id}")
	public ModelAndView getUser(@PathVariable("id") int id) {
		try {
			User u = service.getById(id);
			System.out.println(u);

			if (id < 0)
				return new ModelAndView("error", "msg", "has sido malicioso, tu cuenta ha sido bloqueada");
			if (u == null)
				return new ModelAndView("error", "msg", "el usuario que buscas no esta creado");

			UserForm form = new UserForm();
			ModelAndView modelView = new ModelAndView("showUser", "user", u);
			modelView.addObject("userForm", form);
			return modelView;
		} catch (Exception e) {
			ModelAndView m = new ModelAndView("error");
			m.addObject("msg", "parece ser que el sistema esta caido, por favor, intentelo m�s tarde");
			return m;
		}
	}

	@GetMapping("/admin/user/delete/{id}")
	public ModelAndView deleteUser(@PathVariable("id") int id, HttpSession session) {
		try {
			service.destroy(id);
			ModelAndView m = new ModelAndView("showAll", "users",
					service.getAll((Pagnation) session.getAttribute("page")));
			m.addObject("filterClass", new FilterClass());
			return m;
		} catch (Exception e) {
			e.printStackTrace();
			ModelAndView m = new ModelAndView("error");
			m.addObject("msg", "parece ser que el sistema esta caido, por favor, intentelo m�s tarde");
			return m;
		}
	}

	@GetMapping("/admin/user/delete/confirm/{id}")
	public ModelAndView deleteUserConfirm(@PathVariable("id") int id) {
		User u = service.getById(id);
		return new ModelAndView("deleteConfirm", "user", u);
	}

	@GetMapping("/admin/user")
	public ModelAndView createUser() {
		User u = new User();
		UserForm form = new UserForm();
		ModelAndView modelView = new ModelAndView("showUser", "user", u);
		modelView.addObject("userForm", form);
		return modelView;
	}

	@PostMapping("/admin/user")
	public ModelAndView newUser(@ModelAttribute("userForm") UserForm form, HttpSession session) {

		User u = new User();
		u.setDashid(form.getDashId());
		u.setEmail(form.getEmail());
		u.setForename1(form.getForename1());
		u.setForename2(form.getForename2());
		u.setName(form.getName());
		u.setRol(rolService.getById(form.getRol()));
		u.setStatus(form.getStatus());
		u.setId(form.getId());

		try {
			if (u.getId() < 0)
				service.add(u);
			else
				service.update(u);

			Pagnation page = (Pagnation) session.getAttribute("page");
			ModelAndView m = new ModelAndView("showAll", "users", service.getAll(page));
			m.addObject("filterClass", new FilterClass());
			return m;

		} catch (CannotUpdateUserException e) {
			e.printStackTrace();
			ModelAndView m = new ModelAndView("showUser", "msg",
					"el usuario que has intentado modificar no puede ser modificado");
			m.addObject("userForm", form);
			return m;
		} catch (CannotCreateUserException e) {
			e.printStackTrace();
			ModelAndView m = new ModelAndView("showUser", "msg",
					"el usuario que has intentado crear no puede ser creado");
			m.addObject("userForm", form);
			return m;
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			ModelAndView m = new ModelAndView("showUser", "msg",
					"El dash id ha de tener 7 car�cteres y el formato X000000");
			m.addObject("userForm", form);
			return m;
		} catch (Exception e) {
			ModelAndView m = new ModelAndView("error");
			m.addObject("msg", "parece ser que el sistema esta caido, por favor, intentelo m�s tarde");
			return m;
		}
	}

	@PostMapping("/admin/filter")
	public ModelAndView newUser(@ModelAttribute("filterClass") FilterClass form) {
		if(form.toString().equals("")) {
			form.setDashId(true);
		}
		try {
			ModelAndView m = new ModelAndView("showAll", "users", service.getAll(form.toString()));
			m.addObject("filterClass", new FilterClass());
			return m;
		} catch (Exception e) {
			ModelAndView m = new ModelAndView("error");
			m.addObject("msg", "parece ser que el sistema esta caido, por favor, intentelo m�s tarde");
			return m;
		}
	}

	@GetMapping("/admin/page/{next}")
	public ModelAndView page(@PathVariable("next") int next, HttpSession session) {
		Pagnation page = (Pagnation) session.getAttribute("page");
		if (next == -1)
			page.prevPage();
		if (next == 1)
			page.nextPage();
		try {
			ModelAndView m = new ModelAndView("showAll", "users", service.getAll(page));
			m.addObject("filterClass", new FilterClass());
			return m;
		} catch (Exception e) {
			ModelAndView m = new ModelAndView("error");
			m.addObject("msg", "parece ser que el sistema esta caido, por favor, intentelo m�s tarde");
			return m;
		}
	}
}

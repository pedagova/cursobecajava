package com.atos.formacion.web.controlers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.atos.formacion.Rols;
import com.atos.formacion.exceptions.UserLogNonFoundException;
import com.atos.formacion.exceptions.UserNonActiveException;
import com.atos.formacion.exceptions.UserNonFoundException;
import com.atos.formacion.exceptions.WrongPassException;
import com.atos.formacion.modelo.Pagnation;
import com.atos.formacion.modelo.User;
import com.atos.formacion.modelo.forms.FilterClass;
import com.atos.formacion.modelo.forms.LogginForm;
import com.atos.formacion.services.interfaze.UserService;

@Controller
public class LogginController {

	@Autowired
	private UserService service;

	@GetMapping("/")
	public ModelAndView index(HttpSession session) {
		LogginForm form = new LogginForm();
		return new ModelAndView("logginForm", "logginForm", form);
	}

	@PostMapping("/loggin")
	public ModelAndView loggin(@ModelAttribute("logginForm") LogginForm form, HttpSession session) {
		User u = null;
		try {
			u = service.loggin(form.getName(), form.getPass());
			session.setAttribute("userAct", u);
			Pagnation page = new Pagnation();
			session.setAttribute("page", page);

			if (u.getRol().getName().equals(Rols.USER))
				return new ModelAndView("userView", "user", u);
			if (u.getRol().getName().equals(Rols.ADMIN)) {
				ModelAndView m = new ModelAndView("showAll", "users", service.getAll(page));
				m.addObject("filterClass", new FilterClass());
				return m;
			}

		} catch (UserLogNonFoundException e) {
			form.setPass("");
			return new ModelAndView("createPass", "loginPassForm", form);
		} catch (UserNonFoundException e) {
			form.setPass("");
			ModelAndView m = new ModelAndView("logginForm", "loginForm", form);
			m.addObject("msg", "usuario o contrase�a incorrectos");
			return m;
		} catch (WrongPassException e) {
			form.setPass("");
			ModelAndView m = new ModelAndView("logginForm", "loginForm", form);
			m.addObject("msg", "usuario o contrase�a incorrectos");
			return m;
		} catch (UserNonActiveException e) {
			form.setPass("");
			ModelAndView m = new ModelAndView("logginForm", "loginForm", form);
			m.addObject("msg", "usuario o contrase�a incorrectos");
			return m;
		}catch(Exception e) {
			form.setPass("");
			ModelAndView m = new ModelAndView("logginForm", "loginForm", form);
			m.addObject("msg", "parece ser que el sistema esta caido, por favor, intentelo m�s tarde");
			return m;
		}
		return new ModelAndView("error", "msg", "no eres la mejor persona que he conocido eh ...");

	}

	@PostMapping("/loggin/create")
	public ModelAndView logginCreate(@ModelAttribute("logginForm") LogginForm form, HttpSession session) {
		try {
			User u = service.logginCreate(form.getName(), form.getPass());
			session.setAttribute("userAct", u);
			if (u.getRol().getName().equals(Rols.USER))
				return new ModelAndView("userView", "user", u);
			if (u.getRol().getName().equals(Rols.ADMIN)) {
				session.setAttribute("page", new Pagnation());
				ModelAndView m = new ModelAndView("showAll", "users",
						service.getAll((Pagnation) session.getAttribute("page")));
				m.addObject("filterClass", new FilterClass());
				return m;
			}
		} catch (UserNonFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception e) {
			form.setPass("");
			ModelAndView m = new ModelAndView("logginForm", "loginForm", form);
			m.addObject("msg", "parece ser que el sistema esta caido, por favor, intentelo m�s tarde");
			return m;
		}
		return new ModelAndView("error", "msg", "Has modificado el Dash id sin enviarlo desde un formulario, de esto ya si que no me hago cargo cafre");

	}

}

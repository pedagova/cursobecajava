<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>

	<c:url value="/loggin/create" var="act" />
	<label>Es la primera vez que haces loggin, crea tu pass</label>
	<div class="container">
		<form:form method="POST" action="${act}"
			modelAttribute="loginPassForm" class="form-horizontal">
			
	
			<div class="form-group">
				<label for="dashid">Das id:</label>
				<form:input maxlength="7" path="name" value="${loginPassForm.name}" readonly="true" id="dashid" class="form-control-plaintext"/>
			</div>
			<div class="form-group">
				<label for="pass">Password:</label>
				<form:input path="pass" type="password" class="form-control"
					id="pass" placeholder="Enter password" />
			</div>
			<button type="submit" class="btn btn-primary">Submit</button>
		</form:form>

	</div>

</body>
</html>
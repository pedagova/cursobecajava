<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>

	<c:url value="/loggin" var="act" />

	<div class="container">
	
		<c:if test="${msg!=null}">
			<div class="alert alert-danger" role="alert">
				<strong>Oh snap!</strong> ${msg }
			</div>
		</c:if>
		<div class="alert alert-info" role="alert">
				si es la primera vez que te conectas deja la contraseņa vacia
			</div>
		<form:form method="POST" action="${act}" modelAttribute="logginForm"
			class="form-horizontal">
			<div class="form-group">
				<label for="dashid">Das id:</label>
				<form:input maxlength="7" path="name" value="${logginForm.name}"
					id="dashid" class="form-control" />
			</div>
			<div class="form-group">
				<label for="pass">Password:</label>
				<form:input path="pass" type="password" class="form-control"
					id="pass" placeholder="Enter password" />
			</div>
			<button type="submit" class="btn btn-primary">Submit</button>
		</form:form>

	</div>

</body>
</html>
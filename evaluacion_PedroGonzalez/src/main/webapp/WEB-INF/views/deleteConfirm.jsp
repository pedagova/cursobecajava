<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

</head>
<body>

	<div class=container>
		<label>Seguro que quieres borrar al usuario con el id: ${user.dashid}</label> <br><a
			href="<c:url value = '/admin/user/delete/${user.id}'></c:url>" type="button" class="btn btn-danger">delete
		</a> <a href='<c:url value="/admin/user/all"/>' type = "button" class="btn btn-primary">cancel
		</a>
	</div>
</body>
</html>
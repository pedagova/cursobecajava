<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>

	<div class="container">
		<c:if test="${msg != null}">
			<div class="alert alert-danger" role="alert">
				<strong>Oh snap!</strong> ${msg }
			</div>
		</c:if>
		<c:url value="/admin/user" var="act" />
		<form:form method="POST" action="${act}" modelAttribute="userForm">
			<div class="row">
				<div class="form-group col-md-6">
					<label for="dashid">DashId: </label>
					<form:input path="dashId" value="${user.dashid}"
						readonly="${user.id>=0}" required="true" maxlength="7"
						minlength="7" id="dashid" class="form-control" />
				</div>
				<div class="form-group col-md-6">
					<form:label path="name" for="name">Nombre</form:label>
					<form:input path="name" value="${user.name}" required="true"
						class="form-control" id="name" />
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-6">
					<label for="forename1">Apellido 1: </label>
					<form:input path="forename1" value="${user.forename1}"
						required="true" id="forename1" class="form-control" />
				</div>
				<div class="form-group col-md-6">
					<label for="forename2">Apellido 2: </label>
					<form:input path="forename2" value="${user.forename2}"
						required="true" id="forename2" class="form-control" />
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-6">
					<label for="email">Email: </label>
					<form:input path="email" value="${user.email}" type="email"
						required="true" id="email" class="form-control" />
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-6">
					<form:select path="rol">
						<form:options items="${rols}" itemValue="id" itemLabel="name"
							selected="${user.rol.id}" />
					</form:select>
					<form:select path="status">
						<form:option value="0" label="activo" />
						<form:option value="1" label="no activo" />
					</form:select>
				</div>
			</div>


			<form:input path="id" value="${user.id}" hidden="true" />


			<input type="submit" value="ok" class="btn btn-primary" />
			<a href='<c:url value="/admin/user/all"/>' type="button"
				class="btn btn-danger">cancel</a>
		</form:form>
	</div>

</body>
</html>
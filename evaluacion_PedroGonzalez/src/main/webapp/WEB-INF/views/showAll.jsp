<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<title>Insert title here</title>
</head>
<body>

	<c:url value="/admin/filter" var="act" />
	<div class="container">
		<a href='<c:url value = "/admin/user"/>' type="button"
			class="btn btn-primary">add</a>

		<table class="table">
			<form:form method="POST" action="${act }"
				modelAttribute="filterClass">
				<thead class="thead-dark">
					<tr>


						<th><form:checkbox path="status" /></th>
						<th><form:checkbox path="dashId" /></th>
						<th><form:checkbox path="name" /></th>
						<th><form:checkbox path="forename1" /></th>
						<th><form:checkbox path="forename2" /></th>
						<th><form:checkbox path="rol" /></th>
						<th></th>

					</tr>
				</thead>

				<thead class="thead-dark">
					<tr>
						<th>status</th>
						<th>dashId</th>
						<th>name</th>
						<th>forename 1</th>
						<th>forename 2</th>
						<th>rol</th>
						<th><input type="submit" class="btn btn-primary" /></th>

					</tr>
				</thead>
			</form:form>
			<tbody>
				<c:forEach items="${users}" var="user">
					<tr>
						<td>${user.status}</td>
						<td>${user.dashid}</td>
						<td>${user.name}</td>
						<td>${user.forename1}</td>
						<td>${user.forename2}</td>
						<td>${user.rol.name}</td>
						<c:if test="${userAct.id == user.id }">
							<th hidden="true"></th>
						</c:if>
						<c:if test="${userAct.id != user.id }">
							<th><a type="button" class="btn btn-danger"
								href="<c:url value = '/admin/user/delete/confirm/${user.id}'></c:url>">
									delete </a> <a type="button" class="btn btn-primary"
								href="<c:url value = '/admin/user/${user.id}'></c:url>">
									update </a></th>
						</c:if>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<a href='<c:url value="/admin/page/-1"/>'><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		<a href='<c:url value="/admin/page/1"/>'><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
	</div>

</body>
</html>
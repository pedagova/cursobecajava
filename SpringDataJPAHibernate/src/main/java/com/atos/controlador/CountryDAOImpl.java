package com.atos.controlador;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.atos.modelo.Country;

@Repository
public class CountryDAOImpl extends GenericDAOImpl<Country, String> implements CountryDAO{

	@PersistenceContext
	private EntityManager em;
	
	public CountryDAOImpl() {
		super(Country.class);
	}


	@Override
	public List<Country> findByRegion(int regionId) {
		
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		
		CriteriaQuery<Country> q = cb.createQuery(Country.class);
		Root<Country> c = q.from(Country.class);
//		q.select(c);
//		
//		ParameterExpression<Integer> p = cb.parameter(Integer.class);
//		q.where(cb.equal(regionId, c.get("region")));
		
		q.select(c).where((cb.equal(c.get("region").get("regionId"),regionId)));
		  
		//SELECT * FROM COUNTRY WHERE COUNTRY.REGION.IDREGION=regionId
		return em.createQuery(q).getResultList();
	}


	@Override
	public List<Country> findAll() {
		return findAll("countryId");
	}

}

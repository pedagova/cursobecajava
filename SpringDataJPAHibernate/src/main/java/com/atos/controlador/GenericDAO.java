package com.atos.controlador;

import java.util.List;

import com.atos.modelo.Region;



public interface GenericDAO<T, PK> {

	/**
	 * 
	 * @param country
	 */
	void create(T country);

	/**
	 * 
	 * @param country
	 */
	void edit(T country);

	/**
	 * 
	 * @param pkString
	 */
	void delete(PK pkString);

	/**
	 * 
	 * @param pkString
	 * @return
	 */
	T findById(PK pkString);

	/**
	 * 
	 * @param pkName a string with the field name for the primary key for the class
	 * @return
	 */
	
	List<T> findAll(String pkName);

}

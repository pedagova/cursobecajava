package com.atos.controlador;

import java.util.List;

import com.atos.modelo.Region;

public interface RegionDAO extends GenericDAO<Region, Integer>{
	List<Region> findAll();
}

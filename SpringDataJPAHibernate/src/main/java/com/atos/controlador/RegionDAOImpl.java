package com.atos.controlador;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.atos.modelo.Region;

@Repository
public class RegionDAOImpl extends GenericDAOImpl<Region, Integer> implements RegionDAO{

	
	@PersistenceContext
	private EntityManager em;
	
	public RegionDAOImpl() {
		super(Region.class);
	}

	@Override
	public List<Region> findAll() {
		return findAll("regionId");
	}

}

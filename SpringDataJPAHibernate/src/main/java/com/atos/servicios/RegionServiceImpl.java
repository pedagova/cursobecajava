package com.atos.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.atos.controlador.GenericDAO;
import com.atos.modelo.Region;

/*
 * Configurar componente de Spring mediante anotaciones.
 * 
 * Como esta en la capa de servicios @Service
 * 
 * Aqu� gestionamos las transacciones de los metodos de negocio
 */
@Service
public class RegionServiceImpl implements RegionService {

	/*
	 * Inyectar el dao en el servicio
	 */
	@Autowired
	private GenericDAO<Region, Integer> dao;

	/*
	 * Gestion de transacion por parte de String
	 */
	@Transactional
	@Override
	public void add(Region region) {
		dao.create(region);
	}

	/*
	 * Gestion de transacion por parte de String
	 */
	@Transactional
	@Override
	public void update(Region region) {
		dao.edit(region);
	}

	/*
	 * Gestion de transacion por parte de String
	 */
	@Transactional
	@Override
	public void destroy(int regionId) {
		dao.delete(regionId);
	}

	/*
	 * Gestion de transacion por parte de String.
	 * 
	 * Como es de consulta la marcamos de solo lectura
	 */
	@Transactional(readOnly = true)
	@Override
	public Region getById(int regionId) {
		return dao.findById(regionId);
	}

	/*
	 * Gestion de transacion por parte de String.
	 * 
	 * Como es de consulta la marcamos de solo lectura
	 */
	@Transactional(readOnly = true)
	@Override
	public List<Region> getAll() {
		return dao.findAll("regionId");
	}

}

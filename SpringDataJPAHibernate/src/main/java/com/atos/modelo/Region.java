package com.atos.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/*
 * Entidad JPA para tabla HR.REGIONS
 * 
 */
@Entity
@Table(name="REGIONS", schema="HR")
public class Region implements Serializable {
	private static final long serialVersionUID = 1L;

	public List<Country> getCountries() {
		return countries;
	}

	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}

	@Id
	@Column(name="REGION_ID")
	private int regionId;

	@Column(name="REGION_NAME")
	private String regionName;

	
	@OneToMany(mappedBy = "region", fetch = FetchType.EAGER)
	private List<Country> countries;
	
	public Region() {
	}

	public int getRegionId() {
		return this.regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public String getRegionName() {
		return this.regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	@Override
	public String toString() {
		return "Region [regionId=" + regionId + ", regionName=" + regionName + ", countries=" + countries.toString() + "]";
	}

	
	
	

}
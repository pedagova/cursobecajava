package com.atos.ejecucion;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.atos.modelo.Region;
import com.atos.servicios.RegionService;

public class TestRegion {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring-config.xml");

		RegionService servicio = (RegionService) ctx.getBean("regionServiceImpl");

		List<Region> regions = servicio.getAll();
		System.out.println("\nNumero regiones inicio: " + regions.size());
		System.out.println(regions);

		Region newRegion = new Region();
		newRegion.setRegionId(5);
		newRegion.setRegionName("Talavera");

		servicio.add(newRegion);
		System.out.println("\nRegion insertada");

		Region region2 = servicio.getById(2);
		region2.setRegionName(region2.getRegionName() + "!!!!");
		servicio.update(region2);
		
		regions = servicio.getAll();
		System.out.println("\nNumero regiones actuales: " + regions.size());
		System.out.println(regions);

		servicio.destroy(5);
		
		regions = servicio.getAll();
		System.out.println("\nNumero regiones finales: " + regions.size());
		System.out.println(regions);
		ctx.close();
	}

}

package utilities;

import java.util.regex.Pattern;

public class Validable {
	protected boolean validarSexo(char s) {
		if(s != 'H' && s != 'M') 
			return false;
		return true;
	}
	protected boolean validarDNI(String dni) {
		String dniRegexp = "\\d{8}[A-HJ-NP-TV-Z]";
		
		if(Pattern.matches(dniRegexp, dni))
			return true;
		
		return false;
	}
	protected boolean validarEdad(int edad) {
		if(edad < 0) 
			return false;
		return true;
	}
	protected boolean validarAltura(float altura) {
		if(altura < 0) 
			return false;
		return true;
	}
	protected boolean validarPeso(float peso) {
		if(peso < 0) 
			return false;
		return true;
	}
	protected boolean validarNombre(String nombre) {
		String nameRegexp = "([a-z] | [A-Z])*";
		if(Pattern.matches(nameRegexp, nombre)) {
			return true;
		}
		return false;	
	}
}

package curso.mvc.servicios;

import java.util.List;

import curso.mvc.excepciones.CategoriaException;
import curso.mvc.modelo.CategoriaDTO;

/*
 * Interfaz Facade (Fachada) para acceder al DAO
 */
public interface CategoriaService {

	void add(CategoriaDTO dto) throws CategoriaException;
	
	CategoriaDTO findById(String codigoDTO) throws CategoriaException;
	
	void edit(CategoriaDTO dto) throws CategoriaException;
	
	void remove(String codigoDTO) throws CategoriaException;
	
	List<CategoriaDTO> findAll() throws CategoriaException;
	
}

package curso.mvc.servicios;

import java.util.List;

import curso.mvc.excepciones.CategoriaException;
import curso.mvc.modelo.CategoriaDAO;
import curso.mvc.modelo.CategoriaDTO;

public class CategoriaServiceImpl implements CategoriaService {

	/**
	 * Objeto DAO para trabajar con el modelo
	 */
	private CategoriaDAO dao;
	
	public CategoriaServiceImpl(CategoriaDAO dao) {
		this.dao = dao;
	}

	@Override
	public void add(CategoriaDTO dto) throws CategoriaException {
		dao.create(dto);
	}

	@Override
	public CategoriaDTO findById(String codigoDTO) throws CategoriaException {
		return dao.read(codigoDTO);
	}

	@Override
	public void edit(CategoriaDTO dto) throws CategoriaException {
		dao.update(dto);
	}

	@Override
	public void remove(String codigoDTO) throws CategoriaException {
		dao.delete(codigoDTO);
	}

	@Override
	public List<CategoriaDTO> findAll() throws CategoriaException {
		return dao.readAll();
	}

}

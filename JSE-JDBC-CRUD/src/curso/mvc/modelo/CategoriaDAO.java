package curso.mvc.modelo;

import java.util.List;

import curso.mvc.excepciones.CategoriaException;

/*
 * Interfaz DAO (Data Access Object) para el DTO (CategoriaDTO)
 * 
 * con la funcionalidad (m�todos).
 * 
 * CRUD
 */
public interface CategoriaDAO {

	/*
	 * Crear DTO (INSERT en BBDD)
	 */
	void create(CategoriaDTO dto) throws CategoriaException;
	
	/*
	 * Recuperar DTO (SELECT con CLAUSULA WHERE en BBDD)
	 */
	CategoriaDTO read(String codigoDTO) throws CategoriaException;
	
	/*
	 * Modificar DTO (UPDATE en BBDD)
	 */
	void update(CategoriaDTO dto) throws CategoriaException;
	
	/*
	 * Eliminar DTO (DELETE en BBDD)
	 */
	void delete(String codigoDTO) throws CategoriaException;
	
	/*
	 * Recuperar TODOS los DTO (SELECT en BBDD)
	 */
	List<CategoriaDTO> readAll() throws CategoriaException;
	
}

package curso.mvc.modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import curso.mvc.excepciones.CategoriaException;
import curso.utilidades.JDBC;

/*
 * Clase de implementación del DAO para JDBC sobre BBDD ORACLE
 * 
 * Utilizamos instrucciones parametrizadas (PreparedStatement)
 * 
 */
public class CategoriaDAOImpl implements CategoriaDAO {

	private static final String SELECT = "SELECT job_id, job_title, min_salary, max_salary FROM hr.jobs WHERE job_id = ?";
	private static final String INSERT = "INSERT INTO hr.jobs (job_id,job_title,min_salary,max_salary) VALUES (?,?,?,?)";
	private static final String UPDATE = "UPDATE hr.jobs SET job_title = ?, max_salary = ?, min_salary = ? WHERE job_id = ?";
	private static final String DELETE = "DELETE FROM hr.jobs WHERE job_id = ?";
	private static final String SELECT_ALL = "SELECT job_id, job_title, min_salary, max_salary FROM hr.jobs";

	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(JDBC.URL, JDBC.USER, JDBC.PASSWORD);
	}

	@Override
	public void create(CategoriaDTO dto) throws CategoriaException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(INSERT);

			ps.setString(1, dto.getCodigo());
			ps.setString(2, dto.getCategoria());
			ps.setDouble(3, dto.getSalarioMinimo());
			ps.setDouble(4, dto.getSalarioMaximo());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new CategoriaException("Fallo insercion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new CategoriaException("Fallo cierre insercion en BBDD", e);
				}
			}
		}
	}

	@Override
	public CategoriaDTO read(String codigoDTO) throws CategoriaException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT);

			ps.setString(1, codigoDTO);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				String codigo = rs.getString("job_id");
				String categoria = rs.getString("job_title");
				double salarioMinimo = rs.getDouble("min_salary");
				double salarioMaximo = rs.getDouble("max_salary");

				return new CategoriaDTO(codigo, categoria, salarioMinimo, salarioMaximo);

			} else {
				throw new CategoriaException("No hay categoria con codigo " + codigoDTO);
			}

		} catch (SQLException e) {
			throw new CategoriaException("Fallo lectura en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new CategoriaException("Fallo cierre lectura en BBDD", e);
				}
			}
		}
	}

	@Override
	public void update(CategoriaDTO dto) throws CategoriaException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(UPDATE);

			ps.setString(1, dto.getCategoria());
			ps.setDouble(2, dto.getSalarioMaximo());
			ps.setDouble(3, dto.getSalarioMinimo());
			ps.setString(4, dto.getCodigo());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new CategoriaException("Fallo actualizacion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new CategoriaException("Fallo cierre actualizacion en BBDD", e);
				}
			}
		}
	}

	@Override
	public void delete(String codigoDTO) throws CategoriaException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(DELETE);

			ps.setString(1, codigoDTO);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new CategoriaException("Fallo eliminacion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new CategoriaException("Fallo cierre eliminacion en BBDD", e);
				}
			}
		}
	}

	@Override
	public List<CategoriaDTO> readAll() throws CategoriaException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT_ALL);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				List<CategoriaDTO> categorias = new ArrayList<>();

				String codigo = null;
				String categoria = null;
				double salarioMinimo = 0;
				double salarioMaximo = 0;

				do {
					codigo = rs.getString("job_id");
					categoria = rs.getString("job_title");
					salarioMinimo = rs.getDouble("min_salary");
					salarioMaximo = rs.getDouble("max_salary");

					categorias.add(new CategoriaDTO(
							codigo, categoria, salarioMinimo, salarioMaximo));

				} while (rs.next());

				return categorias;

			} else {
				throw new CategoriaException("No hay categorias");
			}

		} catch (SQLException e) {
			throw new CategoriaException("Fallo lectura completa en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new CategoriaException("Fallo cierre lectura completa en BBDD", e);
				}
			}
		}
	}

}

package demo1_JDBC;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
 import demo1_JDBC.ConexionDB;
 
public class ConexionConsolaJavaOracle {
 
    static Connection con = null;
    static Statement st = null;
    static ResultSet rs = null;
 
    public static void main(String[] args) throws SQLException {
        //Obtenemos la conexion a Oracle
        con = ConexionDB.getConexion();
        String consulta = "SELECT * FROM emp";
        //Preparamos la consulta
        st = con.createStatement();
        //Ejecutamos la consulta
        rs = st.executeQuery(consulta);
 
        System.out.println("C�DIGO\tNOMBRES\tAPELL\tEDAD\tSALARIO\tESTADO");
 
        //Ahora recorremos el Resultset
        while (rs.next()) {
            int codigo, edad, estado;
            String nombres, apellidos;
            double salario;
 
            //Recogemos los datos llamando al nombre de la columna que el
            //Resultset nos ha devuelto
            
            nombres = rs.getString("ename");
            
            //Imprimimos los datos por la consola
            System.out.println(nombres + "\t");
        }
        //Cerramos los elementos usados
        rs.close();
        rs = null;
        st.close();
        st= null;
        con.close();
        con=null;
    }
}
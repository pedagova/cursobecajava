<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.atos.formacion.model.Game"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	
	<div>
		<c:forEach var="i" begin="0" end="${fn:length(game.word) - 1}" step="1">
			<c:set value="_" var="out"/>
			<c:set value="${fn:substring(game.word, i, i+1)}" var="wl"></c:set>
			<c:forEach items="${game.letters}" var="l">
				<c:if test="${l eq wl}">
					<c:set value="${ wl }" var="out"/>
				</c:if>
			</c:forEach>   	
			<c:out value="${out}"></c:out>
		</c:forEach>
		
		
	</div>
	
	<form action="<c:url value = '/play'/>" method="POST">
		<input type = "text" name="letter">
		<input type = "submit" value="ok">
	</form>
	
	<label>Te quedan :</label><c:out value="${Game.MAX_ERRORS - game.errors}"></c:out>
	
	
</body>
</html>
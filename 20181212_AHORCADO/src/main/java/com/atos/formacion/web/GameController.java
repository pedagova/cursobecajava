package com.atos.formacion.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.atos.formacion.model.Game;


@Controller
public class GameController {
	
	@Autowired
	Game game; 
	
	
	@RequestMapping(value = { "/play" }, method = RequestMethod.POST)
	public ModelAndView playLetter(@RequestParam("letter") String letter) {
		game.playLetter(letter.charAt(0));
		if(game.isWinned())
			return new ModelAndView("/win");
		if(game.isLoosed())
			return new ModelAndView("/error");
		return new ModelAndView("/play", "game", game);

	}
	
}

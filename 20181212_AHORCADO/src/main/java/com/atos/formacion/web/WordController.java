package com.atos.formacion.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.atos.formacion.model.Game;

@Controller
public class WordController {
	@Autowired
	Game game;
	
	@RequestMapping(value = { "/change"}, method = RequestMethod.POST)
	public ModelAndView change(@RequestParam("secretWord") String word) {
		game.setWord(word);
		return new ModelAndView("index");

	}
	
	
}

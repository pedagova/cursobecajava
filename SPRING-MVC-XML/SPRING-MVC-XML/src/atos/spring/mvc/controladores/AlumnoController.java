package atos.spring.mvc.controladores;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import atos.spring.mvc.modelo.AlumnoForm;
import atos.spring.mvc.services.AlumnoService;
import atos.spring.mvc.services.AlumnoServiceImpl;

/*
 * Configurar como componente de Spring.
 * 
 * Controlador MVC. Funcionalmente las acciones (clase Java) con
 * la logica de negocio
 */
@Controller
public class AlumnoController {

	@RequestMapping(value = "nuevoAlumno.html", method = RequestMethod.GET)
	public ModelAndView one() {

		return new ModelAndView("alumno", "alumnoForm", 
				new AlumnoForm());

	}

	@RequestMapping(value = "listadoAlumnos.html", method = RequestMethod.POST)
	public ModelAndView two(@ModelAttribute("alumnoForm") AlumnoForm alumno) {

		AlumnoService service = new AlumnoServiceImpl();

		service.add(alumno);

		return new ModelAndView("alumnos", "listado", service.getAll());
	}
	
	@RequestMapping(value = "updateAlumno/{id}.html", method = RequestMethod.GET)
	public ModelAndView three(@PathVariable(value="id") String id) {

		AlumnoService service = new AlumnoServiceImpl();
		AlumnoForm alumn = service.getAlumno(id);
		//REVISARRR
		if(alumn == null)
			return null;
		return new ModelAndView("updateAlumno", "alumno", alumn);
	}
	@RequestMapping(value = "alumno/{id}.html", method = RequestMethod.DELETE)
	public ModelAndView five(@PathVariable(value="id") String id) {

		AlumnoService service = new AlumnoServiceImpl();
		AlumnoForm alumn = service.getAlumno(id);
		if(alumn == null)
			return null;
		return new ModelAndView("listadoAlumno", "listado", service.getAll());
	}
	
	@RequestMapping(value = "updateAlumno.html", method = RequestMethod.POST)
	public ModelAndView four(@ModelAttribute("alumnoForm") AlumnoForm alumno) {

		AlumnoService service = new AlumnoServiceImpl();
		AlumnoForm alumn = service.getAlumno(alumno.getNombre());	
		
		//REVISARRR
		if(alumn == null)
			return null;
		
		service.updateAlumno(alumno);

		
		return new ModelAndView("alumnos", "listado", service.getAll());
	}
}

package atos.spring.mvc.modelo;

import java.io.Serializable;

/*
 * Clase Java (POJO) que encapsula datos de aplicacion
 * 
 * En Spring se denominan FormBean
 * 
 * Vamos a enlazar con formulario de la pagina login.jsp
 */
public class UserForm implements Serializable {

	private static final long serialVersionUID = 666L;

	private String user;
	private String key;

	public UserForm() {
		this.user = "";
		this.key = "";
	}

	public UserForm(String user, String key) {
		this.user = user;
		this.key = key;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}

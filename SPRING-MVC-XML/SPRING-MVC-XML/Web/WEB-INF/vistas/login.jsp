<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    
<%-- Libreria core de JSTL --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- Libreria para formularios HTML de Spring MVC --%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Spring MVC</title>
</head>
<body>
	<%-- Mostrar en la pagina el valor del atributo mensaje cunado sea
		 distinto de ok --%>
		 <c:if test="${mensaje != 'ok'}">
		 	<%-- Mostrar valor atributo --%>
		 	<h3>${mensaje}</h3>
		 </c:if>

	<%-- Crear variable donde guardar la pagina a la que ir --%>
	<c:url var="peticion" value="verificarLogin.html" />

	
	<%-- 
		 Crear formulario de Spring para enlazar los controles
	 	 con las propiedades del objeto del modelo que se 
	 	 ha enviado (usuario) 
	 	 Atributo action tiene el patron de llamada al metodo
	 	 de negocio del controlador que se quiere ejecutar
	--%>
	<form:form id="login" modelAttribute="usuario" method="post"
		action="${peticion}">
	
		<table style="width: 350px; height: 125px">
			<tr>
				<td>
					<%-- Etiqueta de texto asociada a la
						 caja de user --%>
					<form:label path="user">Usuario</form:label>
				</td>
				<td>
					<%-- Caja de texto asociada a la
						 propiedad user del formbean asociado --%>
					<form:input path="user" />
				</td>
			</tr>
			<tr>
				<td>
					<%-- Etiqueta de texto asociada a la
						 caja de clave --%>
					<form:label path="key">Clave</form:label>
				</td>
				<td>
					<%-- Caja de texto asociada a la
						 propiedad key del formbean asociado --%>
					<form:password path="key" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<%-- Boton de envio --%>
					<input type="submit" value="   Login   "/>
				</td>
			</tr>
		</table>
	
	</form:form>
</body>
</html>
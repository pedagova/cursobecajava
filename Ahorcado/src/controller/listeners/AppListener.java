package controller.listeners;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/*
 * Listener (gestor de eventos) para aplicacion (ServletContext)
 */
@WebListener
public class AppListener implements ServletContextListener {

	final String SECRET_WORD = "papaya";
	
	public void contextDestroyed(ServletContextEvent sce) {
		System.out.println("Replegada del servidor la app");
	}

	public void contextInitialized(ServletContextEvent sce) {
		ServletContext context = sce.getServletContext();
		context.setAttribute("secretWord", SECRET_WORD);
	}

}

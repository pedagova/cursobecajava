package controller.actions;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import model.Game;

public class PutLetterAction implements Action{

	@Override
	public String execute(HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		ServletContext context = session.getServletContext();
		
		Game g = (Game) context.getAttribute("game");
		
		boolean ok = g.setLetter(request.getParameter("letter").charAt(0));
		
		if(!ok) {
			g.addError();
		}
		
		if(g.getErrors() == Game.MAX_ERRORS) {
			return "/error.jsp";
		}else if(g.isWinned()){
			return "/win.jsp";
		}else {
			return "/play.jsp";
		}
	}

}

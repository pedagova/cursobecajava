package controller.actions;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class NewWordAction implements Action {

	@Override
	public String execute(HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		ServletContext context = session.getServletContext();
		String s = (String) request.getAttribute("secretWord");
		context.setAttribute("secretWord", s);
		return "/play.jsp";
	}

}

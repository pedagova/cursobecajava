package controller.actions;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import model.Game;

public class RestartAction implements Action{

	@Override
	public String execute(HttpServletRequest request) {
		HttpSession session = request.getSession();
		ServletContext context = session.getServletContext();
		
		context.setAttribute("game", new Game((String)context.getAttribute("secretWord")));
		
		
		return "/play.jsp";
	}

}

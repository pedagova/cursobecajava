package controller.actions;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import model.Game;

public class PlayAction implements Action {

	@Override
	public String execute(HttpServletRequest request) {
		HttpSession session = request.getSession();
		ServletContext context = session.getServletContext();
		
		String s = (String) context.getAttribute("secretWord");
		context.setAttribute("game", new Game(s));
		return "/play.jsp";
	}



}

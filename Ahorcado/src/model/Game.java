package model;

import java.util.ArrayList;
import java.util.List;

public class Game {
	public static final int MAX_ERRORS = 5;
	private String word;
	private List<Character> letters;
	private int errors;
	
	private boolean rightLetter(char l) {
		if(letters.contains(l) || !word.contains(l+"")) {
			return false;
		}
		
		return true;
	}
	/**
	 * 
	 * @param letter
	 * @return returns true if the word contains the letter and u didn't type the letter before
	 */
	public boolean setLetter(char l) {
		if(!rightLetter(l)) {
			if(!word.contains(l+""))
				letters.add(l);
			return false;
		}
		letters.add(l);
		return true;
	}
	
	public Game(String word) {
		this.word = word;
		letters = new ArrayList<Character>();
		errors = 0;;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}
	
	public List<Character> getLetters() {
		return letters;
	}
	public void addError() {
		errors++;		
	}
	public int getErrors() {
		return errors;
	}
	public void setErrors(int errors) {
		this.errors = errors;
	}
	public boolean isWinned() {
		for(int i = 0; i < getWord().length(); i++) {
			if(!getLetters().contains(getWord().charAt(i))){
				return false;
			}
		}
		return true;
	}
	
}

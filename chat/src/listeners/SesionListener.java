package listeners;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/*
 * Listener (gestor de eventos) para sesion (ServletContext)
 */
@WebListener
public class SesionListener implements HttpSessionListener {

	public void sessionCreated(HttpSessionEvent se) {
		System.out.println("sesion created");
	}

	public void sessionDestroyed(HttpSessionEvent se) {
		System.out.println("sesion close");
	}

}

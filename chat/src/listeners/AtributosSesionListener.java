package listeners;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

/*
 * Listener (gestor de eventos) para atributos de sesion 
 * (HttpSession)
 */
@WebListener
public class AtributosSesionListener implements HttpSessionAttributeListener {

	public void attributeAdded(HttpSessionBindingEvent se) {
		System.out.println("atribute added");

	}

	public void attributeRemoved(HttpSessionBindingEvent se) {
		System.out.println("atribute removed");
	}

	public void attributeReplaced(HttpSessionBindingEvent se) {
		System.out.println("atribute replaced");
	}

}
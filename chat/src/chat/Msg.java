package chat;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/msg")
public class Msg  extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		String msg = request.getParameter("msg");
		
		HttpSession session = request.getSession();

		// Recuperar app donde se acaba de crear una sesion
		ServletContext context = session.getServletContext();

		@SuppressWarnings("unchecked")
		List<String> log = (List<String>) context.getAttribute("log");
		log.add("<h3>" + msg + "<h3>");
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/msg.jsp");

		dispatcher.forward(request, response);
	}

}

package model.controller.dao.interfaze;

import java.util.List;

import model.entity.Book;

public interface BookDAO{

	void create(Book book);
	
	void edit(Book book);
	
	void delete(int bookId);
	
	List<Book> findAll();

	Book findById(int i);
	
	List<Book> findByAuthor(int i);
	
}

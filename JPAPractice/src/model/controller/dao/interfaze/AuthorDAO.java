package model.controller.dao.interfaze;

import java.util.List;

import model.entity.Author;

public interface AuthorDAO{

	void create(Author author);
	
	void edit(Author author);
	
	void delete(int authorId);
	
	List<Author> findAll();

	Author findById(int i);
	
}

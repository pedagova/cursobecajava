package model.controller.dao.interfaze;

import java.util.List;

import model.entity.Cover;

public interface CoverDAO{

	void create(Cover cover);
	
	void edit(Cover cover);
	
	void delete(int coverId);
	
	List<Cover> findAll();

	Cover findById(int i);
	
}

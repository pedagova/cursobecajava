package model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="BOOK")
@NamedQueries({
	@NamedQuery(name="Book.findAll", query="select b from Book b"),
	@NamedQuery(name="Book.findByID", query="select b from Book b where b.bid = :nombre"),
	@NamedQuery(name="Book.findByAuthor", query="select b from Book b where b.author = :author")
})
public class Book implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2511513914228437775L;
	
	@Id
	@Column(name="BID")
	private int bid;
	
	@Column(name="TITLE")
	private String title;
	
	@Column(name="REF")
	private int ref;
	
	@Column(name="RDATE")
	private String rdate;
	
	private Author author;
	
	@Column(name="FK_COVER")
	private int cover;
	
	@Override
	public String toString() {
		return "Book [bid=" + bid + ", title=" + title + ", ref=" + ref + ", rdate=" + rdate + ", autor=" + author.getName()
				+ ", cover=" + cover + "]";
	}

	public Book() {
		super();
	}

	public Book(int bid, String title, int ref, String rdate, int cover) {
		super();
		this.bid = bid;
		this.title = title;
		this.ref = ref;
		this.rdate = rdate;

		this.cover = cover;
	}

	public int getBid() {
		return bid;
	}

	public void setBid(int bid) {
		this.bid = bid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getRef() {
		return ref;
	}

	public void setRef(int ref) {
		this.ref = ref;
	}

	public String getRdate() {
		return rdate;
	}

	public void setRdate(String rdate) {
		this.rdate = rdate;
	}

	@ManyToOne(cascade= {}, fetch=FetchType.LAZY)
	@JoinColumn(name="id", unique=false, nullable= true, insertable= true, updatable=true)
	public Author getAutor() {
		return this.author;
	}

	public void setAutor(Author autor) {
		this.author = autor;
	}

	public int getCover() {
		return cover;
	}

	public void setCover(int cover) {
		this.cover = cover;
	}	
}

package model.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity

@Table(name="AUTOR")

@NamedQueries({
	@NamedQuery(name="Author.findAll", query="select a from Author a"),
})
public class Author implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7985263827540366094L;

	@Id
	@Column(name="AID")
	private int id;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="FORENAME")
	private String forename;
	
	private Set<Book> books;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	
	@OneToMany(cascade = {}, fetch = FetchType.LAZY, mappedBy="author")
	public Set<Book> getBooks(){
		return this.books;
	}
	public void setName(String string) {
		this.name = string;
	}
	public String getForename() {
		return forename;
	}
	public void setForename(String forename) {
		this.forename = forename;
	}
	public Author(int id, String name, String forename) {
		super();
		this.id = id;
		this.name = name;
		this.forename = forename;
	}
	@Override
	public String toString() {
		return "Author [id=" + id + ", name=" + name + ", forename=" + forename + "]";
	}
	public Author() {
		super();
	}
	
	
}

package exe;

import model.controller.dao.implement.CoverDAOImpl;
import model.controller.dao.interfaze.CoverDAO;
import model.entity.Cover;

public class TestCover {
	public static void main(String[] args) {
		try {
			// Crear DAO
			CoverDAO dao = new CoverDAOImpl();
			
			System.out.println("Unidad de Persitencia cargada");
			
			// Recuperar entidad por codigo
			Cover cover = dao.findById(1);
			
			// Mostrar datos en pantalla
			mostrar(cover);
			
			// Modificar datos locales
			cover.setCname("Donde cantan los arboles");
			
			// Modificar en DDBB
			dao.edit(cover);
			
			Cover newCover = new Cover(5, "la musica del silencio", "file1");
			
			// Crear cover
			dao.create(newCover);
			
			// Recorrer la lista de coveres
			for(Cover cov : dao.findAll()) {
				mostrar(cov);
			}
			
			// Eliminar cover creada
			dao.delete(newCover.getCid());
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void mostrar(Cover cover) {
		System.out.println(cover.getCid() + "\t" + 
					cover.getCname());		
	}
}

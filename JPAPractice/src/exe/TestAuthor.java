package exe;

import model.controller.dao.implement.AuthorDAOImpl;
import model.controller.dao.interfaze.AuthorDAO;
import model.entity.Author;

public class TestAuthor {

	public static void main(String[] args) {
		
		try {
			// Crear DAO
			AuthorDAO dao = new AuthorDAOImpl();
			
			System.out.println("Unidad de Persitencia cargada");
			
			// Recuperar entidad por codigo
			Author author = dao.findById(1);
			
			// Mostrar datos en pantalla
			mostrar(author);
			
			// Modificar datos locales
			author.setName("loo");
			
			// Modificar en DDBB
			dao.edit(author);
			
			Author newAuthor = new Author(5, "jk", "rowling");
			
			// Crear author
			dao.create(newAuthor);
			
			// Recorrer la lista de authores
			for(Author cov : dao.findAll()) {
				mostrar(cov);
			}
			
			System.out.println("no no no");
			// Eliminar author creada
			dao.delete(newAuthor.getId());
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void mostrar(Author author) {
		System.out.println(author.toString());		
	}
	
}

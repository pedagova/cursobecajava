<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%-- Libreria de etiquetas basica de JSF --%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core"%>

<%-- Libreria de etiquetas HTML de JSF --%>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<!DOCTYPE html>

<%-- 
	 Vista JSF. Vualquier etiqueta JSF (de cualquiera de las
	 2 librerias) van situadas SIEMPRE dentro de la etiqueta
	 f:view
--%>
<f:view>
	<html>
<head>
<meta charset="ISO-8859-1">
<title>Aplicacion Basica JSF</title>
</head>
<body bgcolor="gray" text="black">
	<h3>Datos de registro</h3>
	<%-- Formulario HTML dentro de la vista --%>
	<h:form>
	 	<strong>Nombre</strong>
	 	<%-- 
	 		 Etiqueta de texto enlazada a la propiedad nombre 
	 		 del bean usuario. Utilizamos EL (expression language)
	 	--%>
	 	<h:outputText value="#{usuario.nombre}" />
	 	<br/><br/>
	 	<strong>Email</strong>
	 	<%-- 
	 		 Etiqueta de texto enlazada a la propiedad email 
	 		 del bean usuario. Utilizamos EL (expression language)
	 	--%>
	 	<h:outputText value="#{usuario.email}" />
	 	<br/><br/>
	 	<strong>Nombre</strong>
	 	<%-- 
	 		 Etiqueta de texto enlazada a la propiedad telefono 
	 		 del bean usuario. Utilizamos EL (expression language)
	 	--%>
	 	<h:outputText value="#{usuario.telefono}" />
	 	<br/><br/>
	 	<%--
	 		 Bot�n de env�o. Ejecuta un metodo de negocio de managed
	 		 bean. Una vez finalizada se reenvia a una pagina
	 		 concreta. Esto es una REGLA DE NAVEGACION DINAMICA.
	 	--%>
	 	<h:commandButton action="validar" value="   Enviar Datos   " />
	</h:form>
</body>
	</html>
</f:view>
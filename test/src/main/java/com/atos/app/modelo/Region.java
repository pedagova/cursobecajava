package com.atos.app.modelo;

import java.io.Serializable;
import javax.persistence.*;


/*
 * Entidad JPA para tabla HR.REGIONS
 * 
 */
@Entity
@Table(name="REGIONS", schema="HR")
public class Region implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REGION_ID")
	private int regionId;

	@Column(name="REGION_NAME")
	private String regionName;

	public Region() {
	}

	public int getRegionId() {
		return this.regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public String getRegionName() {
		return this.regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	@Override
	public String toString() {
		return "\nRegion [regionId=" + regionId + ", regionName=" + regionName + "]";
	}
	
	

}
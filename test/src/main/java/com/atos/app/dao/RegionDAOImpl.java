package com.atos.app.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.atos.app.modelo.Region;

/*
 * Configurar componente de Spring mediante anotaciones.
 * 
 * Como es el DAO configurar como repositorio
 */
@Repository
public class RegionDAOImpl implements RegionDAO {

	/*
	 * Inyectar sesion factory de Hibernate
	 */
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(Region region) {
		sessionFactory.getCurrentSession().saveOrUpdate(region);
	}

	@Override
	public void edit(Region region) {
		//TODO
	}

	@Override
	public void delete(int regionId) {
		//TODO
	}

	@Override
	public Region findById(int regionId) {
		//TODO
		return new Region();
	}

	@Override
	public List<Region> findAll() {
		// Consulta HQL
		@SuppressWarnings("unchecked")
		TypedQuery<Region> query =
				sessionFactory.getCurrentSession().
				createQuery("from Region");
				
		return query.getResultList();
	}

}

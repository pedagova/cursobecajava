package com.atos.app.dao;

import java.util.List;

import com.atos.app.modelo.Region;

public interface RegionDAO {

	void create(Region region);

	void edit(Region region);

	void delete(int regionId);

	Region findById(int regionId);

	List<Region> findAll();
}

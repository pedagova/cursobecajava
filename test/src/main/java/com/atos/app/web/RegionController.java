package com.atos.app.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import com.atos.app.modelo.Region;
import com.atos.app.services.RegionService;

/*
 * Componente Spring configurado como controlador MVC
 */
@Controller
public class RegionController {

	/*
	 * Inyectar Servicio
	 */
	@Autowired
	private RegionService service;
	
	/*
	 * Crear atributo asociado al modelo para que se pueda
	 * utilizar desde cualquier metodo del controlador
	 */
	@ModelAttribute("region")
	public Region getRegion() {
		return new Region();
	}
	
	/*
	 * M�todo de negocio para peticion GET y URL /
	 */
	@GetMapping("/")
	public ModelAndView index() {
		return new ModelAndView("index", "regions",
				service.getAll());
	}
	
}

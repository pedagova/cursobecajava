package com.atos.componentes;

import java.util.ArrayList;
import java.util.List;

public class CategoriaDAOImpl implements CategoriaDAO {

	@Override
	public List<Categoria> getCategories() throws CategoriaException {
		return new ArrayList<>();
	}

	@Override
	public Categoria getCategory(int codigo) throws CategoriaException {
		if (codigo >= 10) {
			throw new CategoriaException();
		}
		Categoria categoria = new Categoria();
		categoria.setCodigo(codigo);
		return categoria;
	}

}

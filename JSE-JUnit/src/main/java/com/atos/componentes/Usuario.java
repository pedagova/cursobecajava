package com.atos.componentes;

public class Usuario {

	private boolean validado;
	private int edad;

	public Usuario() {

	}

	public Usuario(boolean validado, int edad) {
		this.validado = validado;
		this.edad = edad;
	}

	public boolean isValidado() {
		return validado;
	}

	public void setValidado(boolean validado) {
		this.validado = validado;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public boolean isMayorEdad() {
		return edad >= 18;
	}

}

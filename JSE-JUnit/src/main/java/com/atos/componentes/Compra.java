package com.atos.componentes;

public class Compra {

	private Usuario usuario;

	private int producto;
	private int unidades;
	private double precio;

	public Compra() {

	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		if (usuario.isValidado() && usuario.isMayorEdad()) {
			this.usuario = usuario;
		} else {
			throw new IllegalArgumentException();
		}
	}

	public int getProducto() {
		return producto;
	}

	public void setProducto(int producto) {
		this.producto = producto;
	}

	public int getUnidades() {
		return unidades;
	}

	public void setUnidades(int unidades) {
		this.unidades = unidades;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public double total() {
		return precio * unidades;
	}

}

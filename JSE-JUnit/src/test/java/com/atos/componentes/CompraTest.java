package com.atos.componentes;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

/*
 * Clase para realizar pruebas unitarias del componente Compra
 * mediante JUnit
 */
public class CompraTest {

	private Usuario usuarioValido;
	private Usuario usuarioNoValido;
	private Compra compra;

	@Before
	public void setUp() {
		// 1.- Arrange (Creacion)
		compra = new Compra();
		compra.setProducto(111);
		compra.setUnidades(10);
		compra.setPrecio(9.99);
		
		// Creacion de mocks (mediante mockito)
		usuarioValido = mock(Usuario.class);
		usuarioNoValido = mock(Usuario.class);
		
		// Programar las llamadas y devoluciones para los objetos mock
		when(usuarioValido.isValidado()).thenReturn(true);
		when(usuarioValido.isMayorEdad()).thenReturn(true);
		
		when(usuarioNoValido.isValidado()).thenReturn(false);
		when(usuarioNoValido.isMayorEdad()).thenReturn(false);
	}
	
	/*
	 * Comprobar funcionalidad metodo setUsuario de Compra con
	 * usuario NO valido
	 */
	@Test(expected=IllegalArgumentException.class)
	public void setTestUsuarioNoValido() {
		compra.setUsuario(usuarioNoValido);
	}
	
	/*
	 * Comprobar funcionalidad metodo setUsuario de Compra con
	 * usuario valido
	 */
	@Test
	public void setTestUsuarioValido() {
		compra.setUsuario(usuarioValido);
		
		// En lugar (o adem�s) de comprobar resultados devueltos
		// Mockito permite comprobar si se han llamado una serie
		// de metodos de un objeto
		verify(usuarioValido).isMayorEdad();
		verify(usuarioValido).isValidado();
				
	}
	
}

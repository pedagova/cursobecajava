package com.atos.componentes;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(value = Parameterized.class)
public class MatematicasTest {

	// Componente a comprobar
	private Matematicas mat;

	/*
	 * Ejecutar justo antes de ejecutar cada metodo de testing
	 */
	@Before
	public void setUp() {
		// 1.- Arrange => Creacion del componente a testear
		mat = new Matematicas();
	}

	/*
	 * Crear un campo para cada parametro del metodo de negocio del componente
	 * (multiplicacion en este caso) que se quiere comprobar
	 */
	private double opeA;
	private double opeB;

	/*
	 * Constructor general para inicializar los campos que se enlazar�n a los
	 * parametros
	 */
	public MatematicasTest(double opeA, double opeB) {
		this.opeA = opeA;
		this.opeB = opeB;
	}

	/*
	 * Crear metodo publico y estatico que devuelve una coleccion (objeto Iterable)
	 * para matriz de una dimension de tipo Object
	 * 
	 * Esta matriz tiene tantos elementos como campos (parametros) hay
	 * 
	 * La coleccion tiene tantos elementos como veces se quiera probar el metodo de
	 * negocio
	 * 
	 * El metodo lleva OBLIGATORIAMENTE la anotacion @Parameters
	 */
	@Parameters
	public static Collection<Object[]> getData() {
		Object[][] parametros = { 
				{ 2.0, 3.0 }, // Primera ejecucion
				{ -2.0, 3.0 }, // Segunda ejecucion
				{ 2.0, -3.0 }, // Tercera ejecucion
				{ -2.0, -3.0 } // Cuarta ejecucion
		};

		return Arrays.asList(parametros);
	}

	/*
	 * Caso de prueba o test para el metodo multiplicacion
	 * 
	 * M�todos publicos con la anotaci�n @Test
	 */
	@Test
	public void pruebaCategoriaOK() throws CategoriaException {
		// 2.- Execution => Fase de ejecuci�n
		double producto = mat.multiplicacion(opeA, opeB);

		// 3.- Validity => Fase de comprobaci�n
		// Comprobar el resultado. Utilizaci�n de aserciones
		// de JUnit
		if ((opeA > 0 && opeB > 0) || (opeA < 0 && opeB < 0)) {
			assertTrue(producto > 0);
		} else {
			assertTrue(producto < 0);
		}

	}

}

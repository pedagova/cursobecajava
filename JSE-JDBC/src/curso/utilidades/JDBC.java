package curso.utilidades;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface JDBC {

	/*
	 * Credenciales conexion al servidor de ORACLE
	 */
	String DRIVERS = "oracle.jdbc.driver.OracleDriver";
	String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	String USER = "HR";
	String PASSWORD = "admin";

	/*
	 * Instrucciones SQL para Test.java
	 */
	String SELECT = "SELECT region_id, region_name " + "FROM regions " + "ORDER BY region_id";

	/*
	 * Instrucciones SQL parametrizada Test5.java
	 */
	String INSERT = "INSERT INTO regions(REGION_ID, REGION_NAME) " + "VALUES(?,?)";

	/*
	 * Instrucciones SQL parametrizada Test5.java
	 */
	String UPDATE = "UPDATE CUENTAS SET SALDO = SALDO + ? WHERE CODIGO = ?";
	
	public static void showRegions(ResultSet rs) throws SQLException {
		if (rs.next()) {
			// Al menos hay un registro
			int codigo = 0;
			String region = null;

			do {
				// Recuperar por nombre el valor de cada
				// columna de la fila apuntada por el cursor
				codigo = rs.getInt("REGION_ID");
				region = rs.getString("REGION_NAME");

				// Mostrar datos en pantalla
				System.out.println(codigo + "\t" + region);

			} while (rs.next()); // Volver a ejecutar mientras haya registros
		} else {
			System.out.println("No se han devuelto registros");
		}
	}

}

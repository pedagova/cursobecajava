package curso.ejecucion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import curso.utilidades.JDBC;

/*
 * TRABAJO CON JDBC
 * 
 * 7 PASOS
 * =======
 * 
 * 1.- AGREGAR JAR CON LOS DRIVERS DEL FACBRICANTE DE BBDD
 * 
 * 2.- CARGAR CLASE CON LOS DRIVERS DE BBDD
 * 
 * 3.- CREAR Y ESTABLECER CONEXION CON LA BBDD
 * 
 * 4.- CREAR OBJETO DE EJECUCION DE SQL
 * 
 * 5.- EJECUTAR CODIGO SQL
 * 
 * 6.- PROCESAR RESULTADOS EJECUCION
 * 
 * 7.- CERRAR CONEXION Y LIBERAR RECURSOS UTILIZADOS
 */
public class Test3 {

	public static void main(String[] args) {

		try {
			// 2.- CARGAR CLASE CON LOS DRIVERS DE BBDD
			Class.forName(JDBC.DRIVERS);

		} catch (ClassNotFoundException e) {
			System.out.println("No se puede cargar la clase" + " con los drivers de BBDD");

			System.exit(0);
		}

		// Conexion a BBDD
		Connection connection = null;

		// Ejecucion instrucciones SQL
		Statement statement = null;

		try {
			// 3.- CREAR Y ESTABLECER CONEXION CON LA BBDD
			// Especificamos URL, USUARIO Y CLAVE
			connection = DriverManager.getConnection(JDBC.URL, JDBC.USER, JDBC.PASSWORD);

			// 4.- CREAR OBJETO DE EJECUCION DE SQL
			// INSTRUCCIONES SQL => Statement
			statement = connection.createStatement();

			// 5.- EJECUTAR CODIGO SQL
			// EXECUTEQUERY => SELECT
			// EXECUTEUPDATE => NO SELECT
			int afectados = statement
					.executeUpdate("INSERT INTO HR.regions(REGION_ID, REGION_NAME) " + " VALUES(5,'ESPA�A')");

			// 6.- PROCESAR RESULTADOS EJECUCION
			// Desplazamiento al primer registro del ResultSet
			if (afectados > 0) {
				System.out.println("Se ha creado una nueva region");
			}

			afectados = statement
					.executeUpdate("DELETE FROM HR.regions WHERE REGION_ID > 4");
			
			if (afectados > 0) {
				System.out.println("Se ha eliminado la region creada");
			}
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();

		} finally {

			// 7.- CERRAR CONEXION Y LIBERAR RECURSOS UTILIZADOS
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.out.println("No se puede cerrar conexion " + " con BBDD");
				}
			}
		}
	}

}

package curso.ejecucion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import curso.utilidades.JDBC;

/*
 * Hacer una transferencia de 5000 de la cuenta 1001 a la
 * cuenta 1007.
 */
public class Test7 {

	public static void main(String[] args) {
		
		try {
			// 2.- CARGAR CLASE CON LOS DRIVERS DE BBDD
			Class.forName(JDBC.DRIVERS);
			
		} catch (ClassNotFoundException e) {
			System.out.println("No se puede cargar la clase"
					+ " con los drivers de BBDD");
			
			System.exit(0);
		}

		// Conexion a BBDD
		Connection connection = null;
		
		try {
			// 3.- CREAR Y ESTABLECER CONEXION CON LA BBDD
			// Especificamos URL, USUARIO Y CLAVE
			connection = DriverManager.getConnection(
					JDBC.URL, JDBC.USER, JDBC.PASSWORD);
			
			PreparedStatement ps = 
					connection.prepareStatement(JDBC.UPDATE);
			
			ps.setInt(1, -5000);
			ps.setInt(2, 1001);
			
			ps.executeUpdate();
			
			ps.setInt(1, 5000);
			ps.setInt(2, 1007);
			
			ps.executeUpdate();
			
			System.out.println("Efectuada la transferencia!!!!");
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		} finally {
		
			// 7.- CERRAR CONEXION Y LIBERAR RECURSOS UTILIZADOS
			if(connection!=null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.out.println("No se puede cerrar conexion "
							+ " con BBDD");
				}
			}
		}
	}

}

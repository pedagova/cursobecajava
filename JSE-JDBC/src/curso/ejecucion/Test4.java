package curso.ejecucion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import curso.utilidades.JDBC;

/*
 * Desplazamiento cursor ResultSet
 * 
 * boolean next() => cursor registro siguiente al actual
 * 
 * boolean first() => cursor primer registro
 * 
 * boolean last() => cursor ultimo registro
 * 
 * boolean previous() => cursor registro anterior al actual
 * 
 * void beforeFirst() => cursor delante del primer registro 
 * 
 * void afterLast() => cursor detr�s del �ltimo registro
 * 
 * 3 tipos de cursores
 * TYPE_FORWARDONLY (PREDETERMINADO) SOLO next()
 * TYPE_SCROLL_INSENSITIVE DESPLAZAMIENTO A CUALQUIER REGISTRO
 * TYPE_SCROLL_SENSITIVE DESPLAZAMIENTO A CUALQUIER REGISTRO
 * 
 * 2 tipos de ResultSet
 * CONCUR_READONLY (PREDETERMINADO) INFO ResultSet s�lo lectura
 * CONCUR_UPDATABLE INFO ResultSet se puede modificar
 * 
 */
public class Test4 {

	public static void main(String[] args) {
		
		try {
			// 2.- CARGAR CLASE CON LOS DRIVERS DE BBDD
			Class.forName(JDBC.DRIVERS);
			
		} catch (ClassNotFoundException e) {
			System.out.println("No se puede cargar la clase"
					+ " con los drivers de BBDD");
			
			System.exit(0);
		}

		// Conexion a BBDD
		Connection connection = null;
		
		// Ejecucion instrucciones SQL
		Statement statement = null;
		
		// Conjunto de resultados ejecucion SELECT
		ResultSet rs = null;
		
		try {
			// 3.- CREAR Y ESTABLECER CONEXION CON LA BBDD
			// Especificamos URL, USUARIO Y CLAVE
			connection = DriverManager.getConnection(
					JDBC.URL, JDBC.USER, JDBC.PASSWORD);
			
			// 4.- CREAR OBJETO DE EJECUCION DE SQL
			// INSTRUCCIONES SQL => Statement
			// Especificamos el tipo de cursor y el tipo
			// de ResultSet
			statement = connection.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			
			// 5.- EJECUTAR CODIGO SQL
			// EXECUTEQUERY => SELECT
			// EXECUTEUPDATE => NO SELECT
			rs = statement.executeQuery(JDBC.SELECT);
		
			// Desplazar a ultimo registro
			rs.last();
			
			// Recuperar numero de ese registro
			int totalRegistros = rs.getRow();
			
			System.out.println("Se van a procesar " + 
					totalRegistros + " registros");
			
			// Situar cursor delante del primer registro
			rs.beforeFirst();
			
			// 6.- PROCESAR RESULTADOS EJECUCION
			// Desplazamiento al primer registro del ResultSet
			if(rs.next()) {
				
				System.out.println("Listado de Regiones");
				
				// Al menos hay un registro
				int codigo = 0;
				String region = null;
				
				do {
					// Recuperar por indice en base 1 el valor de cada
					// columna de la fila apuntada por el cursor
					codigo = rs.getInt(1);
					region = rs.getString(2);
					
					// Mostrar datos en pantalla
					System.out.println(codigo + "\t" + region);
					
				} while(rs.next()); // Volver a ejecutar mientras haya registros
			} else {
				System.out.println("No se han devuelto registros");
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		} finally {
		
			// 7.- CERRAR CONEXION Y LIBERAR RECURSOS UTILIZADOS
			if(connection!=null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.out.println("No se puede cerrar conexion "
							+ " con BBDD");
				}
			}
		}
	}

}

package atos.mvc.filtros;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

/**
 * Filtro configurado para interceptar peticiones realizadas al Servlet
 */
@WebFilter("/Autentification")
public class AutentificationFilter implements Filter {

	public void destroy() {
		System.out.println("Filtro finalizado");
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest requestHttp = (HttpServletRequest) request;

		String name = request.getParameter("name");
		String pass = request.getParameter("pass");
		
		String vista = "/LoginFail.jsp";

		if (validation(name)) {
			chain.doFilter(request, response);
		} else {
			RequestDispatcher dispatcher = 
					request.getRequestDispatcher(vista);

			dispatcher.forward(request, response);
		}
	}

	private boolean validation(String name) {
		String regExp = "[a-z]*";
		return name.matches(regExp);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		System.out.println("Filtro inicializado");
	}

}

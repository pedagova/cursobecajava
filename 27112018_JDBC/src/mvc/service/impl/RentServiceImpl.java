package mvc.service.impl;

import java.util.List;

import mvc.exceptions.RentException;
import mvc.model.dao.interfaze.RentDAO;
import mvc.model.dto.BookDTO;
import mvc.model.dto.RentDTO;
import mvc.service.interfaze.RentService;

public class RentServiceImpl implements RentService {

	private RentDAO rdao;
	
	public RentServiceImpl(RentDAO rdao) {
		this.rdao= rdao;
	}

	@Override
	public void rentBook(int bookID, int clientID) throws RentException {
		rdao.create(new RentDTO(5, bookID, clientID, null, "pasado"));
	}
	
	@Override
	public void returnBook(int rentID) throws RentException {
		rdao.update(rentID);
	}

	@Override
	public List<BookDTO> getPendientBooks(int clientID) throws RentException {
		return rdao.readPendents(clientID);
	}

}

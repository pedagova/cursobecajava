package mvc.service.interfaze;

public interface ClientService {
	/**
	 * 
	 * @param bookID
	 */
	public void rentBook(int bookID);
	
	/**
	 * 
	 * @param bookID
	 */
	public void returnBook(int bookID);
}

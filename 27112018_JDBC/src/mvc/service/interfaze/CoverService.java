package mvc.service.interfaze;

import mvc.exceptions.CoverException;

public interface CoverService {
	
	/**
	 * 
	 * @param bookID
	 * @return the string for the route calculation of the file
	 * @throws CoverException 
	 */
	public String getCover(int bookID) throws CoverException;
}

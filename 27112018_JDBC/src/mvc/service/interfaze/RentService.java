package mvc.service.interfaze;

import java.util.List;

import mvc.exceptions.RentException;
import mvc.model.dto.BookDTO;

public interface RentService {

	public void rentBook(int bookID, int clientID) throws RentException;
	public void returnBook(int rentID) throws RentException;
	public List<BookDTO> getPendientBooks(int i) throws RentException;
	
}

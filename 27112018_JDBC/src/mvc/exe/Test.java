package mvc.exe;

import java.util.List;

import mvc.model.dao.impl.AutorDAOImpl;
import mvc.model.dao.impl.BookDAOImpl;
import mvc.model.dao.impl.CoverDAOImpl;
import mvc.model.dao.impl.RentDAOImpl;
import mvc.model.dao.interfaze.AutorDAO;
import mvc.model.dao.interfaze.BookDAO;
import mvc.model.dao.interfaze.CoverDAO;
import mvc.model.dao.interfaze.RentDAO;
import mvc.model.dto.AutorDTO;
import mvc.model.dto.BookDTO;
import mvc.service.impl.AutorServiceImpl;
import mvc.service.impl.BookServiceImpl;
import mvc.service.impl.CoverServiceImpl;
import mvc.service.impl.RentServiceImpl;
import mvc.service.interfaze.AutorService;
import mvc.service.interfaze.BookService;
import mvc.service.interfaze.CoverService;
import mvc.service.interfaze.RentService;

public class Test {

	public static void main(String[] args) {
		try {
			AutorDAO adao = new AutorDAOImpl();
			BookDAO bdao = new BookDAOImpl();
			RentDAO rdao = new RentDAOImpl();
			CoverDAO cdao = new CoverDAOImpl();
			
			AutorService authorService = new AutorServiceImpl(adao);
			BookService bookService = new BookServiceImpl(bdao, adao);
			RentService rentService = new RentServiceImpl(rdao);
			CoverService coverService = new CoverServiceImpl(cdao);
			
			System.out.println("\n..........get authors.............\n");
			List<AutorDTO> as = authorService.getAll();
			
			for(AutorDTO elem : as) {
				System.out.println(elem.toString());
			}
			
			System.out.println("\n.......get pendent books.........\n");
			List<BookDTO> bs = rentService.getPendientBooks(1);
			
			for(BookDTO elem : bs) {
				System.out.println(elem.toString());
			}
			
			System.out.println("\n.........rent a book............\n");
			rentService.rentBook(1, 1);
			System.out.println("rent ok");
			
			System.out.println("\n.........return a book............\n");
			rentService.returnBook(5);
			System.out.println("return ok");
			new RentDAOImpl().delete(5);
			
			System.out.println("\n..........get books.............\n");
			List<BookDTO> bs2 = bookService.getAll();
			
			for(BookDTO elem : bs2) {
				System.out.println(elem.toString());
			}
			
			System.out.println("\n.......get books by author......\n");
			List<BookDTO> bs3 = bookService.getBooksByAutor(1);
			
			for(BookDTO elem : bs3) {
				System.out.println(elem.toString());
			}
			
			System.out.println("\n.......book's cover......\n");
			String bc= coverService.getCover(1);
			
			System.out.println("\n.......add book with autor......\n");
			bookService.addBook(new BookDTO(100, "el tatuaje azul", 32, "2012-12-12", 1, 1));
			
			System.out.println("book added right");
			
			bookService.deleteBook(100);
			
			System.out.println("\n.......add book without autor......\n");
			bookService.addBook(new BookDTO(100, "el tatuaje azul", 32, "2012-12-12", 99, 1));
			
			System.out.println("book without author added right");
			
			bookService.deleteBook(100);
			authorService.delete(54);
			
		}catch(Exception e) {
			System.out.println(e.getMessage() + "\n");
			e.printStackTrace();
		}
	}

}

package mvc.exceptions;

import java.sql.SQLException;

public class CoverException extends Exception{

	public CoverException(String string) {
		super(string);
	}

	public CoverException(String string, SQLException e) {
		super(string, e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 666L;
	
	

}

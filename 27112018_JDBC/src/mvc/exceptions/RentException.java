package mvc.exceptions;

import java.sql.SQLException;

public class RentException extends Exception{

	public RentException(String string, SQLException e) {
		super(string, e);
	}

	public RentException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 666L;

}

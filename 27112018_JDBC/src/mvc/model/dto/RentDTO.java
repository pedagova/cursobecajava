package mvc.model.dto;

public class RentDTO {
	private int id, book, client;
	private String ddate, rdate;
	@Override
	public String toString() {
		return "RentDTO [id=" + id + ", book=" + book + ", client=" + client + ", ddate=" + ddate + ", rdate=" + rdate
				+ "]";
	}
	public int getBook() {
		return book;
	}
	public void setBook(int book) {
		this.book = book;
	}
	public int getClient() {
		return client;
	}
	public void setClient(int client) {
		this.client = client;
	}
	public String getDdate() {
		return ddate;
	}
	public void setDdate(String ddate) {
		this.ddate = ddate;
	}
	public String getRdate() {
		return rdate;
	}
	public void setRdate(String rdate) {
		this.rdate = rdate;
	}
	public int getId() {
		return id;
	}
	public RentDTO(int id, int book, int client, String ddate, String rdate) {
		super();
		this.id = id;
		this.book = book;
		this.client = client;
		this.ddate = ddate;
		this.rdate = rdate;
	}
	
}

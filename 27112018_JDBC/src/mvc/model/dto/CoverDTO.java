package mvc.model.dto;

import java.io.Serializable;

public class CoverDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 666L;

	private int id;
	private String file, name;
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	@Override
	public String toString() {
		return "CoverDTO [id=" + id + ", file=" + file + ", name=" + name + "]";
	}
	public CoverDTO(int id, String file, String name) {
		super();
		this.id = id;
		this.file = file;
		this.name = name;
	}
}

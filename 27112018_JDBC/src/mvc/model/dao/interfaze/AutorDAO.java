package mvc.model.dao.interfaze;

import java.util.List;

import mvc.exceptions.AutorException;
import mvc.model.dto.AutorDTO;

public interface AutorDAO {

void create(AutorDTO dto) throws AutorException;
	
	/*
	 * Recuperar DTO (SELECT con CLAUSULA WHERE en BBDD)
	 */
	AutorDTO read(int i) throws AutorException;
	
	/*
	 * Modificar DTO (UPDATE en BBDD)
	 */
	void update(AutorDTO dto) throws AutorException;
	
	/*
	 * Eliminar DTO (DELETE en BBDD)
	 */
	void delete(int id) throws AutorException;
	
	/*
	 * Recuperar TODOS los DTO (SELECT en BBDD)
	 */
	List<AutorDTO> readAll() throws AutorException;
	
}

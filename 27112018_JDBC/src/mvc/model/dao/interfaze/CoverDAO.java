package mvc.model.dao.interfaze;

import java.util.List;

import mvc.exceptions.CoverException;
import mvc.model.dto.CoverDTO;

public interface CoverDAO {

	/*
	 * Crear DTO (INSERT en BBDD)
	 */
	void create(CoverDTO dto) throws CoverException;
	
	/*
	 * Recuperar DTO (SELECT con CLAUSULA WHERE en BBDD)
	 */
	CoverDTO read(int bookID) throws CoverException;
	
	/*
	 * Modificar DTO (UPDATE en BBDD)
	 */
	void update(CoverDTO dto) throws CoverException;
	
	/*
	 * Eliminar DTO (DELETE en BBDD)
	 */
	void delete(String codigoDTO) throws CoverException;
	
	/*
	 * Recuperar TODOS los DTO (SELECT en BBDD)
	 */
	List<CoverDTO> readAll() throws CoverException;
}

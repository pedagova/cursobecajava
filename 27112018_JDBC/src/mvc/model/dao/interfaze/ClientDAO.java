package mvc.model.dao.interfaze;

import java.util.List;

import mvc.exceptions.ClientException;
import mvc.model.dto.ClientDTO;

public interface ClientDAO {


	/*
	 * Crear DTO (INSERT en BBDD)
	 */
	void create(ClientDTO dto) throws ClientException;
	
	/*
	 * Recuperar DTO (SELECT con CLAUSULA WHERE en BBDD)
	 */
	ClientDTO read(String idDTO) throws ClientException;
	
	/*
	 * Modificar DTO (UPDATE en BBDD)
	 */
	void update(ClientDTO dto) throws ClientException;
	
	/*
	 * Eliminar DTO (DELETE en BBDD)
	 */
	void delete(String idDTO) throws ClientException;
	
	/*
	 * Recuperar TODOS los DTO (SELECT en BBDD)
	 */
	List<ClientDTO> readAll() throws ClientException;
	
}

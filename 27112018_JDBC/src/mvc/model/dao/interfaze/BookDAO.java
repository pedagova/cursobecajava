package mvc.model.dao.interfaze;

import java.util.List;

import mvc.exceptions.BookException;
import mvc.model.dto.AutorDTO;
import mvc.model.dto.BookDTO;
import mvc.model.dto.CoverDTO;

public interface BookDAO {

	/*
	 * Crear DTO (INSERT en BBDD)
	 */
	void create(BookDTO dto) throws BookException;
	
	/*
	 * Recuperar DTO (SELECT con CLAUSULA WHERE en BBDD)
	 */
	BookDTO read(String idDTO) throws BookException;
	
	/*
	 * Modificar DTO (UPDATE en BBDD)
	 */
	void update(BookDTO dto) throws BookException;
	
	/*
	 * Eliminar DTO (DELETE en BBDD)
	 */
	void delete(int bookId) throws BookException;
	
	/*
	 * Recuperar TODOS los DTO (SELECT en BBDD)
	 */
	List<BookDTO> readAll() throws BookException;

	List<BookDTO> readByAutor(int autorID) throws BookException;

	CoverDTO readCover(int i) throws BookException;

	void createBookAndAuthor(BookDTO book, AutorDTO autorDTO) throws BookException;
}

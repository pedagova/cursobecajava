package mvc.model.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mvc.exceptions.ClientException;
import mvc.model.dao.interfaze.ClientDAO;
import mvc.model.dto.ClientDTO;
import utilities.JDBC;

public class ClientDAOImpl implements ClientDAO{

	private static final String SELECT = "SELECT * FROM client WHERE cid = ?";
	private static final String INSERT = "INSERT INTO client VALUES (?, ?)";
	private static final String UPDATE = "UPDATE client SET forename = ?";
	private static final String DELETE = "DELETE FROM client WHERE cid = ?";
	private static final String SELECT_ALL = "SELECT * FROM client";

	
	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(JDBC.URL, JDBC.USER, JDBC.PASSWORD);
	}
	
	@Override
	public void create(ClientDTO dto) throws ClientException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(INSERT);

			ps.setInt(1, dto.getId());
			ps.setString(2, dto.getName());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new ClientException("Fallo insercion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new ClientException("Fallo cierre insercion en BBDD", e);
				}
			}
		}
	}

	@Override
	public ClientDTO read(String idDTO) throws ClientException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT);

			ps.setString(1, idDTO);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				int id = rs.getInt("aid");
				String name = rs.getString("forename");

				return new ClientDTO(id, name);

			} else {
				throw new ClientException("No hay Autor con id " + idDTO);
			}

		} catch (SQLException e) {
			throw new ClientException("Fallo lectura en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new ClientException("Fallo cierre lectura en BBDD", e);
				}
			}
		}
	}

	@Override
	public void update(ClientDTO dto) throws ClientException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(UPDATE);
			
			ps.setString(2, dto.getName());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new ClientException("Fallo actualizacion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new ClientException("Fallo cierre actualizacion en BBDD", e);
				}
			}
		}
	}

	@Override
	public void delete(String idDTO) throws ClientException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(DELETE);

			ps.setString(1, idDTO);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new ClientException("Fallo eliminacion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new ClientException("Fallo cierre eliminacion en BBDD", e);
				}
			}
		}
	}

	@Override
	public List<ClientDTO> readAll() throws ClientException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT_ALL);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				List<ClientDTO> Autors = new ArrayList<>();

				int id;
				String name = null;
			
				do {
					id = rs.getInt("aid");
					name = rs.getString("forename");

					Autors.add(new ClientDTO(
							id, name));

				} while (rs.next());

				return Autors;

			} else {
				throw new ClientException("No hay Autors");
			}

		} catch (SQLException e) {
			throw new ClientException("Fallo lectura completa en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new ClientException("Fallo cierre lectura completa en BBDD", e);
				}
			}
		}
	}

}

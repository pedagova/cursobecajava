package mvc.model.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mvc.exceptions.CoverException;
import mvc.model.dao.interfaze.CoverDAO;
import mvc.model.dto.CoverDTO;
import utilities.JDBC;

public class CoverDAOImpl implements CoverDAO{

	private static final String SELECT = "SELECT * FROM cover WHERE cid = ?";
	private static final String INSERT = "INSERT INTO cover VALUES (?, ?, ?)";
	private static final String UPDATE = "UPDATE cover SET name = ?, forename = ?";
	private static final String DELETE = "DELETE FROM cover WHERE cid = ?";
	private static final String SELECT_ALL = "SELECT * FROM cover";

	
	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(JDBC.URL, JDBC.USER, JDBC.PASSWORD);
	}
	
	@Override
	public void create(CoverDTO dto) throws CoverException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(INSERT);

			ps.setInt(1, dto.getId());
			ps.setString(2, dto.getName());
			ps.setString(3, dto.getFile());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new CoverException("Fallo insercion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new CoverException("Fallo cierre insercion en BBDD", e);
				}
			}
		}
	}

	@Override
	public CoverDTO read(int idDTO) throws CoverException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT);

			ps.setInt(1, idDTO);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				int id = rs.getInt("cid");
				String name = rs.getString("cname");
				String file = rs.getString("cfile");

				return new CoverDTO(id, name, file);

			} else {
				throw new CoverException("No hay Autor con id " + idDTO);
			}

		} catch (SQLException e) {
			throw new CoverException("Fallo lectura en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new CoverException("Fallo cierre lectura en BBDD", e);
				}
			}
		}
	}

	@Override
	public void update(CoverDTO dto) throws CoverException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(UPDATE);

			ps.setString(1, dto.getName());
			ps.setString(2, dto.getFile());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new CoverException("Fallo actualizacion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new CoverException("Fallo cierre actualizacion en BBDD", e);
				}
			}
		}
	}

	@Override
	public void delete(String idDTO) throws CoverException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(DELETE);

			ps.setString(1, idDTO);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new CoverException("Fallo eliminacion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new CoverException("Fallo cierre eliminacion en BBDD", e);
				}
			}
		}
	}

	@Override
	public List<CoverDTO> readAll() throws CoverException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT_ALL);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				List<CoverDTO> Autors = new ArrayList<>();

				int id;
				String name = null, file = null;
			
				do {
					id = rs.getInt("cid");
					name = rs.getString("cname");
					file = rs.getString("cfile");

					Autors.add(new CoverDTO(
							id, name, file));

				} while (rs.next());

				return Autors;

			} else {
				throw new CoverException("No hay Autors");
			}

		} catch (SQLException e) {
			throw new CoverException("Fallo lectura completa en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new CoverException("Fallo cierre lectura completa en BBDD", e);
				}
			}
		}
	}
}

package mvc.model.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mvc.exceptions.RentException;
import mvc.model.dao.interfaze.RentDAO;
import mvc.model.dto.BookDTO;
import mvc.model.dto.RentDTO;
import utilities.JDBC;

public class RentDAOImpl implements RentDAO{
	
	private static final String SELECT = "SELECT * FROM rent WHERE rid = ?";
	private static final String INSERT = "INSERT INTO rent VALUES (?, ?, ?, ?, ?)";
	private static final String UPDATE = "UPDATE rent SET ddate = ? where rid = ?";
	private static final String DELETE = "DELETE FROM rent WHERE rid = ?";
	private static final String SELECT_ALL = "SELECT * FROM rent";
	private static final String SELECT_PENDENTS = "select b.* from rent r inner join book b on r.fk_book = b.bid where r.ddate is null and r.fk_client = ?";

	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(JDBC.URL, JDBC.USER, JDBC.PASSWORD);
	}
	
	@Override
	public void create(RentDTO dto) throws RentException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(INSERT);

			ps.setInt(1, dto.getId());
			ps.setInt(2, dto.getBook());
			ps.setInt(3, dto.getClient());
			ps.setString(4, dto.getRdate());
			ps.setString(5, dto.getDdate());
			
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new RentException("Fallo insercion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new RentException("Fallo cierre insercion en BBDD", e);
				}
			}
		}
	}

	@Override
	public RentDTO read(String idDTO) throws RentException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT);

			ps.setString(1, idDTO);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				int id = rs.getInt("bid");
				int book = rs.getInt("fk_book");
				int client = rs.getInt("fk_client");
				String rdate = rs.getString("rdate");
				String ddate =  rs.getString("ddate");
				

				return new RentDTO(id, book, client, rdate, ddate);

			} else {
				throw new RentException("No hay Autor con id " + idDTO);
			}

		} catch (SQLException e) {
			throw new RentException("Fallo lectura en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new RentException("Fallo cierre lectura en BBDD", e);
				}
			}
		}
	}

	@Override
	public void update(int rentID) throws RentException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(UPDATE);

			ps.setString(1, "hoy");
			ps.setInt(2, rentID);


			ps.executeUpdate();

		} catch (SQLException e) {
			throw new RentException("Fallo actualizacion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new RentException("Fallo cierre actualizacion en BBDD", e);
				}
			}
		}
	}

	@Override
	public void delete(int idDTO) throws RentException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(DELETE);

			ps.setInt(1, idDTO);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new RentException("Fallo eliminacion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new RentException("Fallo cierre eliminacion en BBDD", e);
				}
			}
		}
	}

	@Override
	public List<RentDTO> readAll() throws RentException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT_ALL);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				List<RentDTO> rents = new ArrayList<>();
			
				do {
					int id = rs.getInt("bid");
					int book = rs.getInt("fk_book");
					int client = rs.getInt("fk_client");
					String rdate = rs.getString("rdate");
					String ddate =  rs.getString("ddate");;
					
					rents.add(new RentDTO(id, book, client, rdate, ddate));

				} while (rs.next());

				return rents;

			} else {
				throw new RentException("No hay books");
			}

		} catch (SQLException e) {
			throw new RentException("Fallo lectura completa en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new RentException("Fallo cierre lectura completa en BBDD", e);
				}
			}
		}
	}

	@Override
	public List<BookDTO> readPendents(int clientID) throws RentException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT_PENDENTS);
			
			ps.setInt(1, clientID);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				List<BookDTO> books = new ArrayList<>();
			
				do {
					int id = rs.getInt("bid");
					String title = rs.getString("title");
					int ref = rs.getInt("ref");
					String date = rs.getString("rdate");
					int autor =  rs.getInt("fk_autor");
					int cover = rs.getInt("fk_cover");

					books.add(new BookDTO(id, title, ref, date, autor, cover));

				} while (rs.next());

				return books;

			} else {
				throw new RentException("No hay books");
			}

		} catch (SQLException e) {
			throw new RentException("Fallo lectura completa en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new RentException("Fallo cierre lectura completa en BBDD", e);
				}
			}
		}
	}

	@Override
	public void rentBook(int clientID, int bookID) {
		// TODO Auto-generated method stub
		
	}

}

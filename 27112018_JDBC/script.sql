create table cover(
    cID number not null,
    cname varchar(32) not null,
    cfile varchar(32) not null,
    primary key (cID)
);

create table autor(
    aID number not null,
    name varchar (32) not null,
    forename varchar (32) not null,
    primary key (aID)
);

create table book(
    bID number not null,
    title varchar(32) not null,
    ref number not null,
    rDate varchar(12),
    fk_autor number  not null,
    fk_cover number,
    PRIMARY KEY (bID),
    FOREIGN key (fk_autor) references autor(aID),
    FOREIGN key (fk_cover) references cover(cID)
);

create table client(
    cID number,
    forename varchar(32),
    PRIMARY KEY (cID)
);

create table rent(
    rID number not null,
    fk_book number not null,
    fk_client number not null,
    rdate varchar(10) not null,
    ddate varchar(10),
    primary key (rID),
    FOREIGN key (fk_client) references client(cID),
    FOREIGN key (fk_book) references book(bID)
);

insert into autor values(1, 'r.a', 'salvatore');
insert into autor values(2, 'laura', 'gallego');

insert into cover values(1, 'el elfo oscuro', 'f22ax');
insert into cover values(2, 'donde cantan los arboles', 'f22ay');

insert into book values(1, 'el elfo oscuro', 42, '1984-06-11', 1, 1);
insert into book values(2, 'donde cantan los arboles', 43, '2014-12-10', 2, 2);

insert into client values (1, 'Pedro');
insert into client values (2, 'David');
insert into client values (3, 'Antonio');


insert into rent values (1, 1, 1, '2010-10-10', null);
--SELECT * FROM autor;

select b.* from rent r inner join book b on r.fk_book = b.bid where r.ddate is null and r.fk_client = 1;


create table bugfixer( id number);

insert into bugfixer values(1);

drop table bugfixer;

<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<%-- Libreria de etiquetas basica de JSF --%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core"%>

<%-- Libreria de etiquetas HTML de JSF --%>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<f:view>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Insert title here</title>
</head>
<body>

<label>nombre: </label>

<h:outputText value="#{data.student.nombre }"></h:outputText>

<label>Email: </label>

<h:outputText value="#{data.student.email }"></h:outputText>

</body>
</html>
</f:view>
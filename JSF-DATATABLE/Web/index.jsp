<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<f:view>
	<html>
<head>
<meta charset="ISO-8859-1">
<title>Ejemplo DataTable Basico JSF</title>
</head>
<body>
	<%--
		Crear DataTable de JSF
		
		Se puede enlazar a objetos que sean Coleccion, RowSet y
		ResultSet
		
		Generar tabla con los datos del objeto fuente (atributo
		items) que se guardan en variable (atributo var)
		
		Va a ganerar una tabla HTML con una fila por cada
		elemento
	--%>
	<h:dataTable var="alumno" value="#{data.students}" border="1"
		rules="all" width="70%" cellpadding="2" cellspacing="10">
		<%--
			Por cada columna se utiliza la etiqueta h:column
		--%>
		<h:column>
			<%--
				Para generar los encabezados de las columnas de
				la tabla se utiliza faceta de nombre header
			
			<f:facet name="header">NOMBRE</f:facet>
--%>
			<%--
				Mostrar propiedad del objeto en componente
			--%>
			<h:outputText value="#{alumno.nombre}" />

			<%--
				Opcionalmente se puede generar un pie en la
				columna con faceta de nombre footer
			
				<f:facet name="footer">NOMBRE</f:facet>
			--%>
		</h:column>

		<h:column>
			<h:outputText value="#{alumno.apellidos}" />
		</h:column>

		<h:column>
			<h:outputText value="#{alumno.email}" />
		</h:column>

		<h:column>
			<h:outputText value="#{alumno.telefono}" />
		</h:column>
	</h:dataTable>
</body>
	</html>
</f:view>
package exe;

import model.controller.dao.implement.UserDAOImpl;
import model.controller.dao.interfaze.UserDAO;
import model.entidad.Pass;
import model.entidad.Rol;
import model.entidad.User;

public class TestUser {
	public static void main(String[] args) {
		
		try {
			
			UserDAO dao = new UserDAOImpl();
			
			System.out.println("Unidad de Persitencia cargada");
			User u = new User(7, "temporal");
			u.setPass(new Pass(32, "1111"));
			u.setRol(new Rol(4, "less than expected"));
			
			dao.create(u);
			
			//dao.delete(7);
			User u2 = dao.findById(7);
			
			System.out.println(u2.toString());
			
			u2 = dao.findById(7);
			
			System.out.println(u.toString());
			
			dao.delete(u.getId());
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
}

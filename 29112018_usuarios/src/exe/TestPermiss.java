package exe;

import model.controller.dao.implement.PermissDAOImpl;
import model.controller.dao.interfaze.PermissDAO;
import model.entidad.Inventory;
import model.entidad.Pass;
import model.entidad.Permiss;
import model.entidad.Rol;

public class TestPermiss {

	public static void main(String[] args) {
try {
			
			PermissDAO dao = new PermissDAOImpl();
			
			System.out.println("Unidad de Persitencia cargada");
			
			
			
			Permiss p = new Permiss(15);
			p.setRol(new Rol(6, "temporal"));
			p.setInventory(new Inventory(9, "prueba"));
			
			dao.create(p);
			
			System.out.println(dao.findById(p.getId()));
			
			
		}catch(Exception e){
			e.printStackTrace();
		}

	}

}

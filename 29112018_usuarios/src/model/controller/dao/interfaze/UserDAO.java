package model.controller.dao.interfaze;

import java.util.List;

import model.entidad.User;

public interface UserDAO{

	void create(User user) throws Exception;
	
	void edit(User user);
	
	void delete(int userId);
	
	List<User> findAll();

	User findById(int i);
	
}

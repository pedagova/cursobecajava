package model.controller.dao.interfaze;

import java.util.List;

import model.entidad.Pass;

public interface PassDAO{

	void create(Pass pass);
	
	void edit(Pass pass);
	
	void delete(int passId);
	
	List<Pass> findAll();

	Pass findById(int i);
	
}

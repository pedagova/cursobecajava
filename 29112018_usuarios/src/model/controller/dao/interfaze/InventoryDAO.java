package model.controller.dao.interfaze;

import java.util.List;

import model.entidad.Inventory;

public interface InventoryDAO{

	void create(Inventory inventory);
	
	void edit(Inventory inventory);
	
	void delete(int inventoryId);
	
	List<Inventory> findAll();

	Inventory findById(int i);
	
}

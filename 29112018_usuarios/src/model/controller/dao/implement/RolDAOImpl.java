package model.controller.dao.implement;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.controller.dao.interfaze.RolDAO;
import model.entidad.Rol;

public class RolDAOImpl implements RolDAO{

	private EntityManagerFactory emf;

	public RolDAOImpl() {
		// Cargar unidad de persistencia
		emf = Persistence.createEntityManagerFactory("PRACTICE2");
	}

	/*
	 * Gestor de Entidades. EntityManager. Se crea SIEMPRE desde
	 * EntityManagerFactory
	 * 
	 * M�todo local (privado) para devolver un EntityManager
	 */
	private EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	@Override
	public void create(Rol pol) {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Abrir transaccion
			em.getTransaction().begin();

			// Crear instancia entidad (hacer INSERT en BBDD
			// asociada al EntityManagerFactory con el que hemos
			// creado nuestro EntityManager)
			em.persist(pol);

			// Confirmar transaccion
			em.getTransaction().commit();

		} 
		catch (Exception e) { System.out.println("Error metodo create");
			  throw e; }
		finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}

	@Override
	public void edit(Rol pol) {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Abrir transaccion
			em.getTransaction().begin();

			// Modificar instancia entidad (hacer UPDATE en BBDD
			// asociada al EntityManagerFactory con el que hemos
			// creado nuestro EntityManager)
			em.merge(pol);

			// Confirmar transaccion
			em.getTransaction().commit();

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}

	@Override
	public void delete(int polId) {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Abrir transaccion
			em.getTransaction().begin();

			// Recuperar instancia de entidad desde EntityManager
			// para que sea ATTACH
			Rol pol = em.getReference(Rol.class, polId);
			
			// Eliminar instancia entidad (hacer DELETE en BBDD
			// asociada al EntityManagerFactory con el que hemos
			// creado nuestro EntityManager)
			em.remove(pol);

			// Confirmar transaccion
			em.getTransaction().commit();

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}


	
	@Override
	public List<Rol> findAll() {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Crear objeto (Query) para ejecutar consulta
			// JPQL estatica (creada con @NamedQuery en el
			// fichero de entidad)
			Query query = em.createNamedQuery("Rol.findAll");

			// Devolver lista entidades 
			return query.getResultList();

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}

	@Override
	public Rol findById(int polId) {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Devolver instancia entidad recuperada por su campo
			// clave
			return em.find(Rol.class, polId);

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}
}

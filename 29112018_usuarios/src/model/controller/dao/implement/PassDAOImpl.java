package model.controller.dao.implement;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.controller.dao.interfaze.PassDAO;
import model.entidad.Pass;

public class PassDAOImpl implements PassDAO{

	private EntityManagerFactory emf;

	public PassDAOImpl() {
		// Cargar unidad de persistencia
		emf = Persistence.createEntityManagerFactory("PRACTICE2");
	}

	/*
	 * Gestor de Entidades. EntityManager. Se crea SIEMPRE desde
	 * EntityManagerFactory
	 * 
	 * M�todo local (privado) para devolver un EntityManager
	 */
	private EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	@Override
	public void create(Pass pass) {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Abrir transaccion
			em.getTransaction().begin();

			// Crear instancia entidad (hacer INSERT en BBDD
			// asociada al EntityManagerFactory con el que hemos
			// creado nuestro EntityManager)
			em.persist(pass);

			// Confirmar transaccion
			em.getTransaction().commit();

		} 
		catch (Exception e) { System.out.println("Error metodo create");
			  throw e; }
		finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}

	@Override
	public void edit(Pass pass) {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Abrir transaccion
			em.getTransaction().begin();

			// Modificar instancia entidad (hacer UPDATE en BBDD
			// asociada al EntityManagerFactory con el que hemos
			// creado nuestro EntityManager)
			em.merge(pass);

			// Confirmar transaccion
			em.getTransaction().commit();

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}

	@Override
	public void delete(int passId) {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Abrir transaccion
			em.getTransaction().begin();

			// Recuperar instancia de entidad desde EntityManager
			// para que sea ATTACH
			Pass pass = em.getReference(Pass.class, passId);
			
			// Eliminar instancia entidad (hacer DELETE en BBDD
			// asociada al EntityManagerFactory con el que hemos
			// creado nuestro EntityManager)
			em.remove(pass);

			// Confirmar transaccion
			em.getTransaction().commit();

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}


	
	@Override
	public List<Pass> findAll() {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Crear objeto (Query) para ejecutar consulta
			// JPQL estatica (creada con @NamedQuery en el
			// fichero de entidad)
			Query query = em.createNamedQuery("Pass.findAll");

			// Devolver lista entidades 
			return query.getResultList();

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}

	@Override
	public Pass findById(int passId) {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Devolver instancia entidad recuperada por su campo
			// clave
			return em.find(Pass.class, passId);

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}
}

package model.entidad;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "PERMISS")
public class Permiss implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2330254957385414603L;

	
	@Id
	@Column(name = "PK_PID")
	private int id;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn (name="FK_RID")
	private Rol rol;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn (name="FK_IID")
	private Inventory inventory;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Permiss [id=" + id + "\nrol=" + rol.toString() + "\ninventory=" + inventory.toString() + "]";
	}

	public Permiss(int id) {
		super();
		this.id = id;
	}

	public Permiss() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}

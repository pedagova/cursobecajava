package model.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="INVENTORY")
public class Inventory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8777116543668324388L;

	@Id
	@Column(name="PK_IID")
	private int id;
	
	@Column(name="INAME")
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Inventory [id=" + id + ", name=" + name + "]";
	}

	public Inventory(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Inventory() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
